import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { LoginsPage } from '../pages/logins/logins';
import { TtdwoPage } from '../pages/ttdwo/ttdwo';
import { HeaderscanPage } from '../pages/headerscan/headerscan';
import { ForgotPage } from '../pages/forgot/forgot';
import { LaporanPage } from '../pages/laporan/laporan';
import { DashboardwoPage } from '../pages/dashboardwo/dashboardwo';
import { DetailscanPage } from '../pages/detailscan/detailscan';
import { ReqsparepartPage } from '../pages/reqsparepart/reqsparepart';
import { AddsparepartPage } from '../pages/addsparepart/addsparepart';
import { AdderrorPage } from '../pages/adderror/adderror';
import { QtyspmodalPage } from '../pages/qtyspmodal/qtyspmodal';
import { FinaltaskPage } from '../pages/finaltask/finaltask';
import {UploadPage} from '../pages/upload/upload';
import { FilePath } from '@ionic-native/file-path';

import { FrhdComponent  } from '../components/frhd/frhd';
import { FrroComponent } from '../components/frro/frro';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ServiceProvider } from '../providers/service/service';
import { MsgProvider } from '../providers/msg/msg';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
//import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { MomentModule } from 'angular2-moment';
import { ApiProvider } from '../providers/api/api';
import { DatabaseProvider } from '../providers/database/database';
import { ImageResizer } from '@ionic-native/image-resizer';
//const config: SocketIoConfig = { url: 'http://192.168.43.83:3000/', options: {} };

import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Geolocation } from '@ionic-native/geolocation';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { SignaturePadModule } from 'angular2-signaturepad';

import { IonicSelectableModule } from 'ionic-selectable';
import { FindarrProvider } from '../providers/findarr/findarr';

import { BerandaPage } from '../pages/beranda/beranda';
import { LocalstoragecontrollerProvider } from '../providers/localstoragecontroller/localstoragecontroller';
import { LogoutmidPage } from '../pages/logoutmid/logoutmid';
import { FrhdPage } from '../pages/frhd/frhd';
import { FrroPage } from '../pages/frro/frro';
import { CodePush, InstallMode, SyncStatus } from '@ionic-native/code-push';
import { SQLite } from '@ionic-native/sqlite';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { ScanlibProvider } from '../providers/scanlib/scanlib';
import { Network } from '@ionic-native/network';

import { InstalasiPage } from "../pages/instalasi/instalasi";
import { ResumeinstalasiPage } from '../pages/resumeinstalasi/resumeinstalasi';
import { ProginstalasiPage } from '../pages/proginstalasi/proginstalasi';
import { KomisioningroPage } from '../pages/komisioningro/komisioningro';
import { KomisioninghdPage } from '../pages/komisioninghd/komisioninghd';
import { FinalinstalasiPage } from '../pages/finalinstalasi/finalinstalasi';
import { KalibrasiPage } from '../pages/kalibrasi/kalibrasi';
import { ResumekalibrasiPage } from '../pages/resumekalibrasi/resumekalibrasi';
import { ProgkalibrasiPage } from '../pages/progkalibrasi/progkalibrasi';
import { FormkalibrasiPage } from '../pages/formkalibrasi/formkalibrasi';
import { FinalkalibrasiPage } from '../pages/finalkalibrasi/finalkalibrasi';

@NgModule({
  declarations: [
    MyApp,
    ForgotPage,
    LaporanPage,
    LoginsPage,
    HeaderscanPage,
    DashboardwoPage,
    DetailscanPage,
    ReqsparepartPage,
    AddsparepartPage,
    AdderrorPage,
    FrroComponent,
    FrhdComponent,
    QtyspmodalPage,
    FinaltaskPage,
    TtdwoPage,
    UploadPage,
    BerandaPage,
    LogoutmidPage,
    FrhdPage,
    FrroPage,
    InstalasiPage,
    ResumeinstalasiPage,
    ProginstalasiPage,
    KomisioningroPage,
    KomisioninghdPage,
    FinalinstalasiPage,
    KalibrasiPage,
    ResumekalibrasiPage,
    ProgkalibrasiPage,
    FormkalibrasiPage,
    FinalkalibrasiPage
  ],
  imports: [
    MomentModule,
    BrowserModule,
  //  SocketIoModule.forRoot(config),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    HttpModule,
    SignaturePadModule,
    IonicSelectableModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LaporanPage,
    ForgotPage,
    HeaderscanPage,
    LoginsPage,
    DashboardwoPage,
    DetailscanPage,
    ReqsparepartPage,
    AddsparepartPage,
    AdderrorPage,
    QtyspmodalPage,
    FinaltaskPage,
    TtdwoPage,
    UploadPage,
    BerandaPage,
    LogoutmidPage,
    FrhdPage,
    FrroPage,
    InstalasiPage,
    ResumeinstalasiPage,
    ProginstalasiPage,
    KomisioningroPage,
    KomisioninghdPage,
    FinalinstalasiPage,
    KalibrasiPage,
    ResumekalibrasiPage,
    ProgkalibrasiPage,
    FormkalibrasiPage,
    FinalkalibrasiPage
  ],
  providers: [
    StatusBar,
    FilePath,
    CodePush,
    ImageResizer,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServiceProvider,
    MsgProvider,
    ApiProvider,
    DatabaseProvider,
    BarcodeScanner,
    NativeGeocoder,
    Geolocation,
    File,
    FileTransfer,
    Camera,
    LocationAccuracy,
    FindarrProvider,
    LocalstoragecontrollerProvider,
    SQLitePorter,
    SQLite,
    ScanlibProvider,
    Network
  ]
})
export class AppModule {}
