import { Component, ViewChild } from '@angular/core';
import { App,Nav, Platform,Loading, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MsgProvider } from '../providers/msg/msg';

import { LoginsPage } from '../pages/logins/logins';
import {UploadPage} from '../pages/upload/upload';
import { timer } from 'rxjs/observable/timer';
import { Storage } from '@ionic/storage';
import { LaporanPage } from '../pages/laporan/laporan';
import { BerandaPage } from '../pages/beranda/beranda';
import { ApiProvider } from '../providers/api/api';
import { DatabaseProvider } from '../providers/database/database';
import { InstalasiPage } from '../pages/instalasi/instalasi';
import { CodePush, SyncStatus } from '@ionic-native/code-push';
import { KalibrasiPage } from '../pages/kalibrasi/kalibrasi';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  loading:Loading;
  rootPage: any = LoginsPage;
  showSplash = true;

  pages: Array<{title: string, component: any}>;

  constructor(
    public db:DatabaseProvider,
    private msg:MsgProvider,
    private storage:Storage,
    public platform: Platform, 
    public statusBar: StatusBar,
    private loadingCtrl:LoadingController, 
    private app:App,
    private api:ApiProvider,
    public codePush: CodePush,
    public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: '- Maintenance', component: BerandaPage },
      { title: '- Instalasi Mesin', component: InstalasiPage},
     { title: '- Kalibrasi', component: KalibrasiPage },
     // { title: 'upload', component:UploadPage},
      { title: 'Logout', component: null }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      timer(5000).subscribe(() => this.showSplash = false)
    });
  }
  dissmissloading(){
    if(this.loading){
        this.loading.dismiss();
        this.loading = null;
    }
  }
  createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  ngOnInit() { 
    this.createLoader('Memuat Data ...');
    this.loading.present().then(() => {
      this.api.postData().then((result) => {
        if(result["message"] == true){
          this.db.createTable();
          this.storage.set('dynamicurl_utama',result["data"].url);
          this.storage.set('apikeyaccess',result["data"].apikeyaccess);
          this.storage.set('module_endpoint',result["data"].module);
          this.storage.get('userdatateknisi').then((val) => {
            if (val != null) { 
              this.dissmissloading();
              this.app.getRootNav().setRoot(BerandaPage);
            } else {
              this.dissmissloading();
              this.app.getRootNav().setRoot(LoginsPage);  
            }
          },(err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          }); 
        }else{
          this.dissmissloading();
          this.msg.msg_plainmsg('Enable get url, please call your IT Support');
        }
        this.dissmissloading();
      });
    setTimeout(() => {
        this.dissmissloading();
      }, 10000);
    }, (err) => {
      this.dissmissloading();
      this.msg.msg_plainmsg(err);
    });
  }
 
  openPage(page) {
    if(page.component){
      this.nav.setRoot(page.component);
    }else{
      this.storage.clear();
      this.app.getRootNav().setRoot(LoginsPage);
    } 
  }
}
