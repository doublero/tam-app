import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
/*
  Name file   : MsgProvider
  Created by  : Alif Razan Saputra 
  Date        : 19/Oktober/2018
*/
@Injectable()
export class MsgProvider {
  private msg: any;
  private btn = [];
  constructor(
    private alertCtrl: AlertController
  ) { }
  msg_plainmsg(param) {
    this.msg = param;
    this.msg_build("ok");
  }
  private msg_build(param) {
    if (param == "ok") {
      this.btn = ['OK'];
    }
    const alert = this.alertCtrl.create({
      title: 'TAM App',
      message: this.msg,
      buttons: this.btn
    });
    alert.present();
  }
}
