import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { MsgProvider } from '../../providers/msg/msg';
import { map } from 'rxjs/operators';

import 'rxjs/add/operator/map';
let apiUrllog = "http://35.240.151.223:43209/middleapi/apimidmobile/geturl"; //url hosting
//let apiUrllog = "http://192.168.43.27:43209/middleapi/apimidmobile/geturl"
@Injectable()
export class ApiProvider {
  public getIp:String;
  constructor(
    public http: Http,
    private msg: MsgProvider
  ) { }

  postData() { //post data berhasil
    let credentials = {
      "access":"TEKNISI",
      "apikeymid": "c4aa32e5d4dc61752ac4c57cb81cf839",
      "apikey":"9d345615fac67581d1dd198f255db73f"
    };
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(apiUrllog , JSON.stringify(credentials), { headers: headers }).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
    });
  }

}
