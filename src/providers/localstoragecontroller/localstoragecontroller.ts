import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { App,Loading, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { MsgProvider } from '../../providers/msg/msg';
import { BerandaPage } from '../../pages/beranda/beranda';
import { LoginsPage } from '../../pages/logins/logins';
import { Storage } from '@ionic/storage';

@Injectable()
export class LocalstoragecontrollerProvider {
  loading: Loading;
  constructor(public http: HttpClient,
    private loadingCtrl: LoadingController,
    private api:ApiProvider,
    private app:App,
    private msg:MsgProvider,
    private storage: Storage) {
     }
  dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  public restart(){
    this.createLoader('Restarting the Application ...');
    this.loading.present().then(() => {
    this.api.postData().then((result) => {
        if(result["message"] == true){
          this.storage.set('dynamicurl_utama',result["data"].url);
          this.storage.set('apikeyaccess',result["data"].apikeyaccess);
          this.storage.set('module_endpoint',result["data"].module);
          this.storage.get('userdatateknisi').then((val) => {
            if (val != null) { 
              this.dissmissloading();
              this.app.getRootNav().setRoot(BerandaPage);
            } else {
              this.dissmissloading();
              this.app.getRootNav().setRoot(LoginsPage);  
            }
          },(err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          }); 
        }else{
          this.dissmissloading();
          this.msg.msg_plainmsg('Enable get url, please call your IT Support');
        }
        this.dissmissloading();
      });
      setTimeout(() => {
        this.dissmissloading();
      }, 3000);
    }, (err) => {
      this.dissmissloading();
      this.msg.msg_plainmsg(err);
    });
  }
  public checkdata(data,callback){
    if(data == "" &&data == null){
      this.createLoader('Restarting the Application ...');
      this.loading.present().then(() => {
      this.api.postData().then((result) => {
          if(result["message"] == true){
            this.storage.set('dynamicurl_utama',result["data"].url);
            this.storage.set('apikeyaccess',result["data"].apikeyaccess);
            this.storage.set('module_endpoint',result["data"].module);
            this.storage.get('userdatateknisi').then((val) => {
              if (val != null) { 
                this.dissmissloading();
                this.app.getRootNav().setRoot(BerandaPage);
              } else {
                this.dissmissloading();
                this.app.getRootNav().setRoot(LoginsPage);  
              }
            },(err) => {
              this.dissmissloading();
              this.msg.msg_plainmsg(err);
            }); 
          }else{
            this.dissmissloading();
            this.msg.msg_plainmsg('Enable get url, please call your IT Support');
          }
          this.dissmissloading();
        });
        setTimeout(() => {
          this.dissmissloading();
        }, 5000);
      }, (err) => {
        this.dissmissloading();
        this.msg.msg_plainmsg(err);
      });
    }else{
      callback(null,true);
    }
  }
}
