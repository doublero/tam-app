//import { Injectable } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { MsgProvider } from '../../providers/msg/msg';


//@Injectable()
export class ScanlibProvider {
  public lat:any;
  public lng:any;
  public lokasi:any;
  
  static get parameters() {
    return [
      [BarcodeScanner],
      [LocationAccuracy],
      [Geolocation],
      [NativeGeocoder],
      [MsgProvider]
    ]
  }
  constructor(
    public scanner,
    public locationAccuracy,
    public geo,
    public nativeGeocoder,
    public msg
    ) {
    console.log('Hello ScanlibProvider Provider');
  } 
  public scanbarcodeonline(){
    var datapos = new Promise(function(resolve, reject){
      this.locationAccuracy.canRequest().then((canRequest: boolean) => {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          () => {
            this.geo.getCurrentPosition().then((pos) => {
              this.lat = pos.coords.latitude;
              this.lng = pos.coords.longitude;
              this.nativeGeocoder.reverseGeocode(this.lat, this.lng)
                .then((result: NativeGeocoderReverseResult[]) => {
                 this.lokasi = JSON.stringify(result);
                  this.scanner.scan().then(barcodeData => {
                    if (barcodeData.text != "") {
                      resolve({
                        "barcode":barcodeData.text,
                        "lng":this.lng,
                        "lat":this.lat,
                        "lokasi":this.lokasi
                      });
                    }
                  }).catch(err => {
                    reject(err);
                  });
                })
                .catch((error: any) => {
                  reject(error);
                });
            }).catch((error) => {
              reject(error);
            });
          },
          error => {
            reject(error);
          }
        );
    });
    });
    return datapos;
  }
  scanbarcode(){
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        () => {
          this.geo.getCurrentPosition().then((pos) => {
            this.lat = pos.coords.latitude;
            this.lng = pos.coords.longitude;
            this.nativeGeocoder.reverseGeocode(this.lat, this.lng)
              .then((result: NativeGeocoderReverseResult[]) => {
               this.lokasi = JSON.stringify(result);
                this.scanner.scan().then(barcodeData => {
                  if (barcodeData.text != "") {
                    return {
                      "barcode":barcodeData.text,
                      "lng":this.lng,
                      "lat":this.lat,
                      "lokasi":this.lokasi
                    }
                  }
                }).catch(err => {
                  return err;
                });
              })
              .catch((error: any) => {
                return error;
              });
          }).catch((error) => {
            return error;
          });
        },
        error => {
          return error;
        }
      );
  });
  }
}
