import { Component, ViewChild, OnInit, Renderer } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { App, ToastController, NavController, LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoginsPage } from '../../pages/logins/logins';
import { ServiceProvider } from '../../providers/service/service';
import { DetailscanPage } from '../../pages/detailscan/detailscan';
import { MsgProvider } from '../../providers/msg/msg';
@Component({
  selector: 'frhd',
  templateUrl: 'frhd.html'
})
export class FrhdComponent {
  @ViewChild("cchd") CardContentHD: any;
  hdform: FormGroup;
  accordionExpanded = false;
  visible = false;
  loading: Loading;
  private id_scan_hd: number;
  
 
  private id_scan_header;
  private kalibrasi;
  private powertime;
  private no_versi_soft;
  private tgl_instalasi;
  private venous_pressure;
  private dialisat_pressure;
  private arteri_pressure;
  private temperature;
  private conductivity;
  private  selang_drain;
  private  selang_inlet;
  private  filter_air_supply;
  private blood_leak;
  private flow_meter;
  private tubing;
  private valve;
  private flow_sensor;
  private pump;
  private tubing_rinse_acid;
  private tubing_disinfektan;
  private dialisat_time;
  private supply_time;
  private rinse_acid_intended;
  private rinse_acid_original;
  private disinfektan_intended;
  private disinfektan_original;
  private rinse_process_w1;
  private rinse_process_dis;
  private rinse_process_w2;
  private catatan;

  constructor(
    private det:DetailscanPage,
    private renderer: Renderer,
    private storage: Storage,
    private service: ServiceProvider,
    private app:App,
    private msg:MsgProvider,
    private formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private toast: ToastController
  ) {
    this.hdform = this.formBuilder.group({
      kalibrasi: [''],
      id_scan_header: [this.getIdscanheader()],
      venous_pressure: ['',Validators.required],
      dialisat_pressure: ['',Validators.required],
      arteri_pressure: ['',Validators.required],
      temperature: ['',Validators.required],
      conductivity: ['',Validators.required],
      selang_drain: ['',Validators.required],
      selang_inlet: ['',Validators.required],
      filter_air_supply: ['',Validators.required],
      blood_leak: ['',Validators.required],
      flow_meter: ['',Validators.required],
      tubing: ['',Validators.required],
      valve: ['',Validators.required],
      flow_sensor: ['',Validators.required],
      pump: ['',Validators.required],
      tubing_rinse_acid: ['',Validators.required],
      tubing_disinfektan: ['',Validators.required],
      dialisat_time: ['',Validators.required],
      supply_time: ['',Validators.required],
      rinse_acid_intended: ['',Validators.required],
      rinse_acid_original: ['',Validators.required],
      disinfektan_intended: ['',Validators.required],
      disinfektan_original: ['',Validators.required],
      rinse_process_w1: ['',Validators.required],
      rinse_process_dis: ['',Validators.required],
      rinse_process_w2: ['',Validators.required],
      catatan: ['',Validators.required],
      powertime: ['',Validators.required],
      no_versi_soft: ['',Validators.required],
      tgl_instalasi: ['',Validators.required]
      });
    this.hdform = new FormGroup({
      kalibrasi: new FormControl(),
      id_scan_header: new FormControl(),
      venous_pressure: new FormControl(),
      arteri_pressure: new FormControl(),
      dialisat_pressure: new FormControl(),
      temperature: new FormControl(),
      conductivity: new FormControl(),
      selang_drain: new FormControl(),
      selang_inlet: new FormControl(),
      filter_air_supply: new FormControl(),
      blood_leak: new FormControl(),
      flow_meter: new FormControl(),
      tubing: new FormControl(),
      valve: new FormControl(),
      flow_sensor: new FormControl(),
      pump: new FormControl(),
      tubing_rinse_acid: new FormControl(),
      tubing_disinfektan: new FormControl(),
      dialisat_time: new FormControl(),
      supply_time: new FormControl(),
      rinse_acid_intended: new FormControl(),
      rinse_acid_original: new FormControl(),
      disinfektan_intended: new FormControl(),
      disinfektan_original: new FormControl(),
      rinse_process_w1: new FormControl(),
      rinse_process_dis: new FormControl(),
      rinse_process_w2: new FormControl(),
      catatan: new FormControl(),
      powertime: new FormControl(),
      no_versi_soft: new FormControl(),
      tgl_instalasi: new FormControl()
    });
    this.setIdscanheader(this.det.getIdscanheader());
      
    
    this.createLoader('Generating data ...');
    this.loading.present().then(() => {
      this.storage.get('dynamicurl_utama').then((val) => {
        if (val != null) {
          let datapost = {
            "apikey": "660d46013c32ad4998e94f5db2e5048e",
            id_scan_header: this.getIdscanheader()
          };
          this.service.postData(datapost, val, "/users/wo/reportscan/hd/loaddata").then((result) => {
            if(result["data"] != ""){
              let dataget = result["data"][0];
              this.setid_scan_hd(dataget["Id_scan_hd"]);
              this.id_scan_header = dataget["id_scan_header"];
              if(dataget["kalibrasi"] == 1){
                this.kalibrasi = true;
              }else{
                this.kalibrasi = false;
              }
              this.powertime = dataget["powertime"];
              this.no_versi_soft = dataget["no_versi_soft"];
              this.tgl_instalasi = dataget["tgl_instalasi"];
              this.venous_pressure = dataget["venous_pressure"];
              this.dialisat_pressure = dataget["dialisat_pressure"];
              this.arteri_pressure = dataget["arteri_pressure"];
              this.temperature = dataget["temperature"];
              this.conductivity = dataget["conductivity"];
              this.selang_drain = dataget["selang_drain"];
              this.selang_inlet = dataget["selang_inlet"];
              this.filter_air_supply = dataget["filter_air_supply"];
              this.blood_leak = dataget["blood_leak"];
              this.flow_meter = dataget["flow_meter"];
              this.tubing = dataget["tubing"];
              this.valve = dataget["valve"];
              this.flow_sensor = dataget["flow_sensor"];
              this.pump = dataget["pump"];
              this.tubing_rinse_acid = dataget["tubing_rinse_acid"];
              this.tubing_disinfektan = dataget["tubing_disinfektan"];
              this.dialisat_time = dataget["dialisat_time"];
              this.supply_time = dataget["supply_time"];
              this.rinse_acid_intended = dataget["rinse_acid_intended"];
              this.rinse_acid_original = dataget["rinse_acid_original"];
              this.disinfektan_intended = dataget["disinfektan_intended"];
              this.disinfektan_original = dataget["disinfektan_original"];
              this.rinse_process_w1 = dataget["rinse_process_w1"];
              this.rinse_process_dis = dataget["rinse_process_dis"];
              this.rinse_process_w2 = dataget["rinse_process_w2"];
              this.catatan = dataget["catatan"];
              this.dissmissloading();
            }else{
              this.kalibrasi = false;
              this.dissmissloading();
            }
          });
          this.dissmissloading();
        }else{
          this.dissmissloading();
          this.storage.clear();
          this.app.getRootNav().setRoot(LoginsPage);
        }
      });
      this.dissmissloading();
      setTimeout(() => {
        this.dissmissloading();
      }, 7000);
    }, (err) => {
      this.dissmissloading();
      this.msg.msg_plainmsg(err);
    });
  }
  setIdscanheader(id_scan_headerv:any){
    this.id_scan_header = id_scan_headerv;
  }
  getIdscanheader():any{
    return this.id_scan_header;
  }
  public createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  public dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  ngOnInit() {
    this.renderer.setElementStyle(this.CardContentHD.nativeElement, "webkitTransition", "max-height 800ms,padding 500ms");
  }
  toggleAccordionHD() {
    if (this.accordionExpanded) {
      this.renderer.setElementStyle(this.CardContentHD.nativeElement, "max-height", "0px");
      this.renderer.setElementStyle(this.CardContentHD.nativeElement, "padding", "0px 16px");
    } else {
      this.renderer.setElementStyle(this.CardContentHD.nativeElement, "max-height", "3200px");
      this.renderer.setElementStyle(this.CardContentHD.nativeElement, "padding", "13px 16px");
    }
    this.visible = !this.visible;
    this.accordionExpanded = !this.accordionExpanded;
  }
  tutup() {
    this.renderer.setElementStyle(this.CardContentHD.nativeElement, "max-height", "0px");
    this.renderer.setElementStyle(this.CardContentHD.nativeElement, "padding", "0px 16px");
    this.visible = !this.visible;
    this.accordionExpanded = !this.accordionExpanded;
  }
  loaddata(){
   
  }
  simpanformhd(){
    this.createLoader('Menyimpan Data ... ');
    this.loading.present().then(() => {
      this.storage.get('dynamicurl_utama').then((val) => {
        if (val != null) {
          let datapost = {
            "data": this.hdform.value,
            "id_scan_hd":this.getid_scan_hd(),
            "apikey": "660d46013c32ad4998e94f5db2e5048e"
          };
          if(this.getid_scan_hd() != null){
            this.service.postData(datapost, val, "/users/wo/reportscan/hd/update").then((result) => {
                this.msg.msg_plainmsg(JSON.stringify(result));
                console.log(result);
                this.dissmissloading();
              });
          }else{
            this.service.postData(datapost, val, "/users/wo/reportscan/hd/save").then((result) => {
              this.dissmissloading();
              this.toast.create({ message: result["data"], duration: 3000, position: 'buttom' }).present();
            });
          }
          this.renderer.setElementStyle(this.CardContentHD.nativeElement, "max-height", "0px");
          this.renderer.setElementStyle(this.CardContentHD.nativeElement, "padding", "0px 16px");
          this.visible = false;
          this.accordionExpanded = !this.accordionExpanded;
          
        }else{
          this.dissmissloading();
          this.storage.clear();
          this.app.getRootNav().setRoot(LoginsPage);
        }
    });
  setTimeout(() => {
        this.dissmissloading();
      }, 8000);
    }, (err) => {
      this.dissmissloading();
      this.msg.msg_plainmsg(err);
    });
  }

  public getid_scan_hd(): number {
    return this.id_scan_hd;
  }
  public setid_scan_hd(value: number) {
    this.id_scan_hd = value;
  }
}
