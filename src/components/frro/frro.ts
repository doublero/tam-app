import { Component, ViewChild, OnInit, Renderer } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { App, ToastController, NavController, LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoginsPage } from '../../pages/logins/logins';
import { ServiceProvider } from '../../providers/service/service';
import { DetailscanPage } from '../../pages/detailscan/detailscan';
import { MsgProvider } from '../../providers/msg/msg';
@Component({
  selector: 'frro',
  templateUrl: 'frro.html'
})
export class FrroComponent {
  private roform: FormGroup;
  loading: Loading;
  visible = false;
  @ViewChild("ccro") cardContentRO: any;
  accordionExpanded = false;



  private id_scan_ro: number;
  private kalibrasi;
  private id_scan_header;
  private powertime;
  private no_versi_soft;
  private tgl_instalasi;
  private arteri_pressure;
  private feed_pressure;
  private system_pressure;
  private permeate_flow;
  private permeate_temperature;
  private permeate_conductivity;
  private concentrate_flow;
  private concentrate_conductivity;
  private feed_water_flow;
  private feed_water_conductivity;
  private feed_water_temperature;
  private leaking_ro;
  private pompa_tekanan_tinggi;
  private instrumen_listrik;
  private mechanical_instrument;
  private ganti_module_membrane;
  private backwash_sand_filter;
  private backwash_carbon_filter;
  private regenerasi_resin;
  private check_kebocoran_piping;
  private penggantian_filter_cartridges;
  private penggantian_sand_filter;
  private penggantian_resin_filter;
  private penggantian_carbon_filter;
  private check_kebocoran_pompa_filtrasi;
  private check_instrumen_listrik;
  private check_kebocoran_pompa_feed;
  private check_instrumen_mekanical;
  private rencana;
  private catatan;



  constructor(
    private det:DetailscanPage,
    private renderer: Renderer,
    private storage: Storage,
    private service: ServiceProvider,
    private app:App,
    private msg:MsgProvider,
    private formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private toast: ToastController
  ) {
    this.roform = this.formBuilder.group({
      kalibrasi: [''],
      id_scan_header: [this.getIdscanheader()],
      feed_pressure: ['', Validators.required],
      system_pressure: ['', Validators.required],
      permeate_flow: ['', Validators.required],
      arteri_pressure: ['', Validators.required],
      permeate_temperature: ['', Validators.required],
      permeate_conductivity: ['', Validators.required],
      concentrate_flow: ['', Validators.required],
      concentrate_conductivity: ['', Validators.required],
      feed_water_flow: ['', Validators.required],
      feed_water_conductivity: ['', Validators.required],
      feed_water_temperature: ['', Validators.required],
      leaking_ro: ['', Validators.required],
      pompa_tekanan_tinggi: ['', Validators.required],
      instrumen_listrik: ['', Validators.required],
      mechanical_instrument: ['', Validators.required],
      ganti_module_membrane: ['', Validators.required],
      backwash_sand_filter: ['', Validators.required],
      backwash_carbon_filter: ['', Validators.required],
      regenerasi_resin: ['', Validators.required],
      check_kebocoran_piping: ['', Validators.required],
      penggantian_filter_cartridges: ['', Validators.required],
      penggantian_sand_filter: ['', Validators.required],
      penggantian_resin_filter: ['', Validators.required],
      penggantian_carbon_filter: ['', Validators.required],
      check_kebocoran_pompa_filtrasi: ['', Validators.required],
      check_instrumen_listrik: ['', Validators.required],
      check_kebocoran_pompa_feed: ['', Validators.required],
      check_instrumen_mekanical: ['', Validators.required],
      rencana: ['', Validators.required],
      catatan: ['', Validators.required],
      powertime: ['', Validators.required],
      no_versi_soft: ['', Validators.required],
      tgl_instalasi: ['', Validators.required]
    });
    this.roform = new FormGroup({
      kalibrasi: new FormControl(),
      id_scan_header: new FormControl(),
      feed_pressure: new FormControl(),
      arteri_pressure: new FormControl(),
      system_pressure: new FormControl(),
      permeate_flow: new FormControl(),
      permeate_temperature: new FormControl(),
      permeate_conductivity: new FormControl(),
      concentrate_flow: new FormControl(),
      concentrate_conductivity: new FormControl(),
      feed_water_flow: new FormControl(),
      feed_water_conductivity: new FormControl(),
      feed_water_temperature: new FormControl(),
      leaking_ro: new FormControl(),
      pompa_tekanan_tinggi: new FormControl(),
      instrumen_listrik: new FormControl(),
      mechanical_instrument: new FormControl(),
      ganti_module_membrane: new FormControl(),
      backwash_sand_filter: new FormControl(),
      backwash_carbon_filter: new FormControl(),
      regenerasi_resin: new FormControl(),
      check_kebocoran_piping: new FormControl(),
      penggantian_filter_cartridges: new FormControl(),
      penggantian_sand_filter: new FormControl(),
      penggantian_resin_filter: new FormControl(),
      penggantian_carbon_filter: new FormControl(),
      check_kebocoran_pompa_filtrasi: new FormControl(),
      check_instrumen_listrik: new FormControl(),
      check_kebocoran_pompa_feed: new FormControl(),
      check_instrumen_mekanical: new FormControl(),
      rencana: new FormControl(),
      catatan: new FormControl(),
      powertime: new FormControl(),
      no_versi_soft: new FormControl(),
      tgl_instalasi: new FormControl()
    });
    this.setIdscanheader(this.det.getIdscanheader());
    
    this.createLoader('Generating data ...');
    this.loading.present().then(() => {
      this.storage.get('dynamicurl_utama').then((val) => {
        if (val != null) {
          let datapost = {
            "apikey": "660d46013c32ad4998e94f5db2e5048e",
            id_scan_header: this.getIdscanheader()
          };
          this.service.postData(datapost, val, "/users/wo/reportscan/ro/loaddata").then((result) => {
            if(result["data"] != ""){
              let dataget = result["data"][0];
              this.setid_scan_ro(dataget["Id_scan_ro"]);
              this.id_scan_header = dataget["id_scan_header"];
              if(dataget["kalibrasi"] == 1){
                this.kalibrasi = true;
              }else{
                this.kalibrasi = false;
              }
             this.kalibrasi=dataget["kalibrasi"];
             this.id_scan_header=dataget["id_scan_header"];
             this.powertime=dataget["powertime"];
             this.no_versi_soft=dataget["no_versi_soft"];
             this.tgl_instalasi=dataget["tgl_instalasi"];
             this.arteri_pressure=dataget["arteri_pressure"];
             this.feed_pressure=dataget["feed_pressure"];
             this.system_pressure=dataget["system_pressure"];
             this.permeate_flow=dataget["permeate_flow"];
             this.permeate_temperature=dataget["permeate_temperature"];
             this.permeate_conductivity=dataget["permeate_conductivity"];
             this.concentrate_flow=dataget["concentrate_flow"];
             this.concentrate_conductivity=dataget["concentrate_conductivity"];
             this.feed_water_flow=dataget["feed_water_flow"];
             this.feed_water_conductivity=dataget["feed_water_conductivity"];
             this.feed_water_temperature=dataget["feed_water_temperature"];
             this.leaking_ro=dataget["leaking_ro"];
             this.pompa_tekanan_tinggi=dataget["pompa_tekanan_tinggi"];
             this.instrumen_listrik=dataget["instrumen_listrik"];
             this.mechanical_instrument=dataget["mechanical_instrument"];
             this.ganti_module_membrane=dataget["ganti_module_membrane"];
             this.backwash_sand_filter=dataget["backwash_sand_filter"];
             this.backwash_carbon_filter=dataget["backwash_carbon_filter"];
             this.regenerasi_resin=dataget["regenerasi_resin"];
             this.check_kebocoran_piping=dataget["check_kebocoran_piping"];
             this.penggantian_filter_cartridges=dataget["penggantian_filter_cartridges"];
             this.penggantian_sand_filter=dataget["penggantian_sand_filter"];
             this.penggantian_resin_filter=dataget["penggantian_resin_filter"];
             this.penggantian_carbon_filter=dataget["penggantian_carbon_filter"];
             this.check_kebocoran_pompa_filtrasi=dataget["check_kebocoran_pompa_filtrasi"];
             this.check_instrumen_listrik=dataget["check_instrumen_listrik"];
             this.check_kebocoran_pompa_feed=dataget["check_kebocoran_pompa_feed"];
             this.check_instrumen_mekanical=dataget["check_instrumen_mekanical"];
             this.rencana=dataget["rencana"];
             this.catatan=dataget["catatan"];
             this.dissmissloading();
            }else{
              this.kalibrasi = false;
              this.dissmissloading();
            }
         
           
          });
          this.dissmissloading();
        }else{
          this.dissmissloading();
          this.storage.clear();
          this.app.getRootNav().setRoot(LoginsPage);
        }
        this.dissmissloading();
      });
      setTimeout(() => {
        this.dissmissloading();
      }, 7000);
    }, (err) => {
      this.dissmissloading();
      this.msg.msg_plainmsg(err);
    });
  }
  public createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  public dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  ngOnInit() {
    this.renderer.setElementStyle(this.cardContentRO.nativeElement, "webkitTransition", "max-height 800ms,padding 500ms");
  }
  toggleAccordionRO() {
    if (this.accordionExpanded) {
      this.renderer.setElementStyle(this.cardContentRO.nativeElement, "max-height", "0px");
      this.renderer.setElementStyle(this.cardContentRO.nativeElement, "padding", "0px 16px");
    } else {
      this.renderer.setElementStyle(this.cardContentRO.nativeElement, "max-height", "5000px");
      this.renderer.setElementStyle(this.cardContentRO.nativeElement, "padding", "13px 16px");
    }
    this.visible = !this.visible;
    this.accordionExpanded = !this.accordionExpanded;
  }
  tutup() {
    this.renderer.setElementStyle(this.cardContentRO.nativeElement, "max-height", "0px");
    this.renderer.setElementStyle(this.cardContentRO.nativeElement, "padding", "0px 16px");
    this.visible = !this.visible;
    this.accordionExpanded = !this.accordionExpanded;
  }
  setIdscanheader(id_scan_headerv:any){
    this.id_scan_header = id_scan_headerv;
  }
  getIdscanheader():any{
    return this.id_scan_header;
  }

  simpanformro(){
    this.createLoader('Menyimpan Data ... ');
    this.loading.present().then(() => {
      this.storage.get('dynamicurl_utama').then((val) => {
        if (val != null) {
          let datapost = {
            "data": this.roform.value,
            "id_scan_ro":this.getid_scan_ro(),
            "apikey": "660d46013c32ad4998e94f5db2e5048e"
          };
          if(this.getid_scan_ro() != null){
            this.service.postData(datapost, val, "/users/wo/reportscan/ro/update").then((result) => {
                this.msg.msg_plainmsg(JSON.stringify(result));
                console.log(result);
                this.dissmissloading();
              });
          }else{
            this.service.postData(datapost, val, "/users/wo/reportscan/ro/save").then((result) => {
              this.msg.msg_plainmsg(JSON.stringify(result));
              this.dissmissloading();
              this.toast.create({ message: result["data"], duration: 3000, position: 'buttom' }).present();
            });
          }
         
          /*if(this.getid_scan_ro() != null){
            this.service.postData(datapost, val, "/users/wo/reportscan/hd/update").then((result) => {
                this.msg.msg_plainmsg(JSON.stringify(result));
                console.log(result);
                this.dissmissloading();
              });
          }else{
            this.service.postData(datapost, val, "/users/wo/reportscan/ro/save").then((result) => {
              this.dissmissloading();
              this.toast.create({ message: result["data"], duration: 3000, position: 'buttom' }).present();
            });
          }*/
          this.renderer.setElementStyle(this.cardContentRO.nativeElement, "max-height", "0px");
          this.renderer.setElementStyle(this.cardContentRO.nativeElement, "padding", "0px 16px");
          this.visible = false;
          this.accordionExpanded = !this.accordionExpanded;
          
        }else{
          this.dissmissloading();
          this.storage.clear();
          this.app.getRootNav().setRoot(LoginsPage);
        }
    });
    setTimeout(() => {
        this.dissmissloading();
      }, 8000);
    }, (err) => {
      this.dissmissloading();
      this.msg.msg_plainmsg(err);
    });
  }


  public getid_scan_ro(): number {
    return this.id_scan_ro;
  }
  public setid_scan_ro(value: number) {
    this.id_scan_ro = value;
  }
}
