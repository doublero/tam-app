import { Component } from '@angular/core';
import { App, NavController, ActionSheetController, ToastController, NavParams, Platform, LoadingController, Loading } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Storage } from '@ionic/storage';
import { FilePath } from '@ionic-native/file-path';
import { MsgProvider } from '../../providers/msg/msg';
import { LoginsPage } from '../../pages/logins/logins';
import { LocalstoragecontrollerProvider } from '../../providers/localstoragecontroller/localstoragecontroller';
import { FindarrProvider } from '../../providers/findarr/findarr';

declare var cordova: any;

import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';
@Component({
  selector: 'page-upload',
  templateUrl: 'upload.html',
})
export class UploadPage {
  public data: any;apikeyaccess: any;
  public datawo: any; userdata:any;
  public jenis: any;menus: any;
  public kode_karyawanz: any;
  lastImage: string = null;
  loading: Loading;
  public id_dashboard_detail: any;
  constructor(
    private imageResizer: ImageResizer,
    private app: App,
    public navCtrl: NavController,
    private camera: Camera,
    private localctrl:LocalstoragecontrollerProvider,
    private file: File,
    private transfer: FileTransfer,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public platform: Platform,
    public loadingCtrl: LoadingController,
    public filePath: FilePath,
    private msg: MsgProvider,
    private storage: Storage,
    public navParams: NavParams,
    private findarr: FindarrProvider
  ) {
    this.data = this.navParams.get('data');
    this.jenis = this.navParams.get('jenis');
    this.kode_karyawanz = this.navParams.get('Kode_Karyawan');
    this.datawo = this.navParams.get('datawo');
    this.id_dashboard_detail = this.navParams.get('id_dashboard_detail');
    this.storage.get('apikeyaccess').then((val) => {
      this.apikeyaccess = val;
    });
    this.storage.get('userdatateknisi').then((val) => {
      this.userdata = val[0];
       });
    this.storage.get('module_endpoint').then((val) => {
      this.menus = val;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadPage');
  }
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }
  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
  }
  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }
  public uploadImage() {
    var targetPath = this.pathForImage(this.lastImage);
    var tmpnaama = this.lastImage;
    var filename = '';
    if (this.jenis == "mesin") {
      filename = "mesin" + tmpnaama;
    } else if (this.jenis == "upload") {
      filename = "upload" + tmpnaama;
    } else if (this.jenis == "uploadmid") {
      filename = "uploadmid" + tmpnaama;
    }
    let options2 = {
      uri: targetPath,
      folderName: 'Protonet',
      quality: 80,
      width: 700,
      height: 700
    } as ImageResizerOptions;
    this.imageResizer
      .resize(options2)
      .then((filePath: string) => {
        var options = {
          fileKey: "file",
          fileName: filename,
          chunkedMode: false,
          mimeType: "multipart/form-data",
          params: {
            "data": this.data,
            "jenis": this.jenis,
            "apikey": this.apikeyaccess,
            "id_teknisi": this.kode_karyawanz,
            "datawo": this.datawo,
            "id_dashboard_detail": this.id_dashboard_detail
          }
        }
        this.storage.get('dynamicurl_utama').then((val) => {
          if (val != null) {
            var nextsegment = null;
            if (this.findarr.searchmenu("UploadMid", this.menus) != null) {
              if(this.jenis == "mesin"){
                nextsegment = this.findarr.searchmenu("UploadMesin", this.menus);
              }else if(this.jenis == "uploadmid"){
                nextsegment = this.findarr.searchmenu("UploadMid", this.menus);
              }else if(this.jenis == "upload"){
                nextsegment = this.findarr.searchmenu("UploadBukti", this.menus);
              } 
              const fileTransfer: FileTransferObject = this.transfer.create();
              this.loading = this.loadingCtrl.create({
                content: 'Uploading...',
              });
              this.loading.present();
              fileTransfer.upload(filePath,val+nextsegment["endpoint"], options).then(data => {
                this.loading.dismissAll();
                this.presentToast('Image succesful uploaded.');
              },err =>{
                this.msg.msg_plainmsg(JSON.stringify(err));
                this.loading.dismissAll()
                this.presentToast('Error while uploading file.');
              });
            }else{
              this.localctrl.restart();
            }
          }else{
            this.localctrl.restart();
          }
        });
      }).catch(e => {
        this.msg.msg_plainmsg(JSON.stringify(e))
      });
  }
  /*
  public uploadImage() {
    
    var targetPath = this.pathForImage(this.lastImage);
    var tmpnaama = this.lastImage;
    var filename = '';
    if(this.jenis == "mesin"){
      filename = "mesin"+tmpnaama;
    }else if(this.jenis == "upload"){
      filename = "upload"+tmpnaama;
    }else if(this.jenis == "uploadmid"){
      filename = "uploadmid"+tmpnaama;
    }
     
    
  
    let options2 = {
      uri: targetPath,
      folderName: 'Protonet',
      quality: 80,
      width: 700,
      height: 700
     } as ImageResizerOptions;
     this.imageResizer
     .resize(options2)
     .then((filePath: string) => {
        var options = {
          fileKey: "file",
          fileName: filename,
          chunkedMode: false,
          mimeType: "multipart/form-data",
          params : {
            "data": this.data,
            "jenis":this.jenis,  
            "apikey": "660d46013c32ad4998e94f5db2e5048e",
            "id_teknisi":this.kode_karyawanz,
            "datawo":this.datawo,
            "id_dashboard_detail":this.id_dashboard_detail
          }
        };
        let jenisurl:any;
  
        if(this.jenis == 'mesin'){
          jenisurl = '/users/wo/uploadphoto';
        }else{
          jenisurl = '/users/wo/uploadphoto'
        }
        const fileTransfer: FileTransferObject = this.transfer.create();
        
        
        this.loading = this.loadingCtrl.create({
          content: 'Uploading...',
        });
        this.loading.present();
        this.storage.get('dynamicurl_utama').then((val) => {
          if (val != null) {
            fileTransfer.upload(filePath, val+jenisurl, options).then(data => {
              this.loading.dismissAll()
              this.presentToast('Image succesful uploaded.');
            }, err => {
              this.msg.msg_plainmsg(JSON.stringify(err));
              this.loading.dismissAll()
              this.presentToast('Error while uploading file.');
            });
          }else{
            this.dissmissloading();
            this.storage.clear();
            this.app.getRootNav().setRoot(LoginsPage);
          }
        });
     })
     .catch(e => {
       this.msg.msg_plainmsg(JSON.stringify(e))
     });
  }*/
  dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
}
