import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FrhdPage } from './frhd';

@NgModule({
  declarations: [
    FrhdPage,
  ],
  imports: [
    IonicPageModule.forChild(FrhdPage),
  ],
})
export class FrhdPageModule {}
