import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading,ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LocalstoragecontrollerProvider } from '../../providers/localstoragecontroller/localstoragecontroller';
import { FindarrProvider } from '../../providers/findarr/findarr';
import { MenuController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { MsgProvider } from '../../providers/msg/msg';

@IonicPage()
@Component({
  selector: 'page-frhd',
  templateUrl: 'frhd.html',
})
export class FrhdPage {
  frhd = {}//array
  loading: Loading;
  menus: any;
  private userdata: any;
  apikeyaccess: any;
  id_scan_header: any;
  constructor(
    private toast:ToastController,
    private msg: MsgProvider,
    private service: ServiceProvider,
    private menuCtrl: MenuController,
    private localctrl: LocalstoragecontrollerProvider,
    private findarr: FindarrProvider,
    private loadingCtrl: LoadingController,
    private storage: Storage,
    public navCtrl: NavController, public navParams: NavParams) {
    this.id_scan_header = this.navParams.get('id_scan_header');
    this.storage.get('module_endpoint').then((val) => {
      this.menus = val;
    });
    this.storage.get('userdatateknisi').then((val) => {
      this.userdata = val;
    });
    this.storage.get('apikeyaccess').then((val) => {
      this.apikeyaccess = val;
    });


    this.menuCtrl.enable(true, 'myMenu');
  }
  public createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  public dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  filldata(data) {
    if(data.data != ""){

    var vdata = data.data[0];
    this.frhd["id_scan_hd"] = vdata["Id_scan_hd"];
    this.frhd["id_scan_header"] = vdata["id_scan_header"];
    this.frhd["kalibrasi"] = vdata["kalibrasi"];
    this.frhd["powertime"] = vdata["powertime"];
    this.frhd["no_versi_soft"] = vdata["no_versi_soft"];
    this.frhd["tgl_instalasi"] = vdata["tgl_instalasi"];
    this.frhd["venous_pressure"] = vdata["venous_pressure"];
    this.frhd["dialisat_pressure"] = vdata["dialisat_pressure"];
    this.frhd["arteri_pressure"] = vdata["arteri_pressure"];
    this.frhd["temperature"] = vdata["temperature"];
    this.frhd["conductivity"] = vdata["conductivity"];
    this.frhd["selang_drain"] = vdata["selang_drain"];
    this.frhd["selang_inlet"] = vdata["selang_inlet"];
    this.frhd["filter_air_supply"] = vdata["filter_air_supply"];
    this.frhd["blood_leak"] = vdata["blood_leak"];
    this.frhd["flow_meter"] = vdata["flow_meter"];
    this.frhd["tubing"] = vdata["tubing"];
    this.frhd["valve"] = vdata["valve"];
    this.frhd["flow_sensor"] = vdata["flow_sensor"];
    this.frhd["pump"] = vdata["pump"];
    this.frhd["tubing_rinse_acid"] = vdata["tubing_rinse_acid"];
    this.frhd["tubing_disinfektan"] = vdata["tubing_disinfektan"];
    this.frhd["dialisat_time"] = vdata["dialisat_time"];
    this.frhd["supply_time"] = vdata["supply_time"];
    this.frhd["rinse_acid_intended"] = vdata["rinse_acid_intended"];
    this.frhd["rinse_acid_original"] = vdata["rinse_acid_original"];
    this.frhd["disinfektan_intended"] = vdata["disinfektan_intended"];
    this.frhd["disinfektan_original"] = vdata["disinfektan_original"];
    this.frhd["rinse_process_w1"] = vdata["rinse_process_w1"];
    this.frhd["rinse_process_dis"] = vdata["rinse_process_dis"];
    this.frhd["rinse_process_w2"] = vdata["rinse_process_w2"];
    this.frhd["catatan"] = vdata["catatan"];
    }
  }
  loaddata() {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("LoadHdScan", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("LoadHdScan", this.menus);
          let datapost = {
            "apikey": this.apikeyaccess,
            "id_scan_header": this.id_scan_header
          };
          this.createLoader('Loading ...');
          this.loading.present().then(() => {
            this.service.postData(datapost, val, nextsegment["endpoint"]).then((result) => {
              this.dissmissloading();
              this.filldata(result);
            });
            setTimeout(() => {
              this.dissmissloading();
            }, 3000);
          }, (err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          });
        } else {
          this.localctrl.restart();
        }
      } else {
        this.localctrl.restart();
      }
    });
  }


  savehd() {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        var nextsegment2 = null;
        if (this.findarr.searchmenu("SaveHdScan", this.menus) != null || this.findarr.searchmenu("UpdateHdScan", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("SaveHdScan", this.menus);
          nextsegment2 = this.findarr.searchmenu("UpdateHdScan", this.menus);
          let datapost = {
            "data": this.frhd,
            "id_scan_hd": this.frhd["id_scan_hd"],
            "id_scan_header": this.id_scan_header,
            "apikey": this.apikeyaccess,
          };
          this.createLoader('Loading ...');
          this.loading.present().then(() => {
           if (datapost["id_scan_hd"] == null) {
            this.service.postData(datapost, val, nextsegment["endpoint"]).then((result) => {
              if(result["message"]==true){
                this.toast.create({ message: result["data"], duration: 3000, position: 'top' }).present();
                
                this.navCtrl.pop();
              }else{
                this.msg.msg_plainmsg(result["data"]);
              }
              this.dissmissloading();
            });
  
          }else if(datapost["id_scan_hd"] != null) {
            this.service.postData(datapost, val, nextsegment2["endpoint"]).then((result) => {
              if(result["message"]==true){
                this.toast.create({ message: result["data"], duration: 3000, position: 'top' }).present();
                this.navCtrl.pop();
              }else{
                this.msg.msg_plainmsg(result["data"]);
              }
              this.dissmissloading();
            });
           }
            setTimeout(() => {
              this.dissmissloading();
            }, 3000);
          }, (err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          });
        } else {
          this.localctrl.restart();
        }
      } else {
        this.localctrl.restart();
      }
    });


}
ionViewDidLoad() {
  this.loaddata();
}

}
