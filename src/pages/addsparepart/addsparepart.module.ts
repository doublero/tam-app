import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddsparepartPage } from './addsparepart';
//import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  declarations: [
    AddsparepartPage,
  ],
  imports: [
    IonicPageModule.forChild(AddsparepartPage),
   // IonicSelectableModule
  ],
})
export class AddsparepartPageModule {}
