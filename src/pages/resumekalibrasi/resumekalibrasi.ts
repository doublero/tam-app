import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { MsgProvider } from '../../providers/msg/msg';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { ProgkalibrasiPage } from '../progkalibrasi/progkalibrasi';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

/**
 * Generated class for the ResumekalibrasiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resumekalibrasi',
  templateUrl: 'resumekalibrasi.html',
})
export class ResumekalibrasiPage {

  lat: any;
  lang: any;
  lokasi: any;

  public loading: Loading;
  id_wo : any;
  no_wo: any;
  id_wd: any;
  STATUS: any;
  STATUS_instalasi: any;
  closed_by: any;
  closed_time: any;
  open_by: any;
  open_time: any;
  kode_customer: any;
  datecreated: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public msg: MsgProvider,
    public http: Http,
    public locationAccuracy: LocationAccuracy,
    public geo: Geolocation,
    public scanner: BarcodeScanner,
    public nativeGeocoder: NativeGeocoder
    ) {
  }

  ionViewDidEnter() {
    this.getData();
  }
  
  getData(){
    this.createLoader('Memuat Data ...');
    this.loading.present().then(() => {
      this.dissmissloading();
      var nowo = this.navParams.get('nowo');
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.get('http://35.240.151.223:43209/apikalibrasi/get-resume-kalibrasi/'+nowo).map(res => res.json()).subscribe(hasil => {
        var status = hasil['status'];
        var data = hasil['data'];
        if (status == true && data.length > 0 && data != null) {
          this.id_wo = data[0].id_wo;
          this.no_wo= data[0].no_wo;
          this.id_wd= data[0].id_wd;
          this.STATUS= data[0].STATUS;
          this.STATUS_instalasi= data[0].STATUS_instalasi;
          this.closed_by= data[0].closed_by;
          this.closed_time= data[0].closed_time;
          this.open_by= data[0].open_by;
          this.open_time= data[0].open_time;
          this.kode_customer= data[0].kode_customer;
          this.datecreated = data[0].datecreated;
        }else{
          this.msg.msg_plainmsg('Data kosong');
        }
      });
    }, (err) => {
      this.dissmissloading();
      this.msg.msg_plainmsg('Gagal Memuat data harap ulangi');
    });
  }

  createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  refresh(event){
    // this.datawo = null;
    this.id_wo = null;
    this.no_wo= null;
    this.id_wd= null;
    this.STATUS= null;
    this.STATUS_instalasi= null;
    this.closed_by= null;
    this.closed_time= null;
    this.open_by= null;
    this.open_time= null;
    this.kode_customer= null;
    this.datecreated = null;
    this.getData();
    setTimeout(() => {
      console.log('berhasil merefresh data');
      event.complete();
    }, 1000);
  }

  cekStatus1(value) {
    let color = new String;
    if (value == '0') {//0 = UNREAD    ABU-ABU
      color = "onnotyet";
    } else if (value == '1') {//2 = PROGRESS	BIRU
      color = "onproccess";
    } else if (value == '2') {//   3 = CLOSE		  MERAH
      color = "onclose";
    }
    return color;
  }

  cekStatus2(value) {
    let txt = new String;
    if (value == '0') { //0 = blm proses    ABU-ABU
      txt = "NOT YET";
    } else if (value == '1') { //2 = PROGRESS	BIRU
      txt = "ON PROCESS";
    } else if (value == '2') { //   3 = CLOSE		MERAH
      txt = "CLOSED";
    }
    return txt;
  }

  btnstatus(param){
    let status = new Boolean;
    if (param == '1' && param != null && param != "") {
      status = false;
    } else {
      status = true;
    }
    return status;
  }

  btnprogress(param){
    let status = new Boolean;
    if (param == '1' && param != null && param != "") {
      status = true;
    } else {
      status = false;
    }
    return status;
  }

  progresskalibrasi(){
    this.navCtrl.push(ProgkalibrasiPage, {"nowo": this.no_wo });
  }

  closedwo(param){
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        () => {
          this.geo.getCurrentPosition().then((pos) => {
            this.lat = pos.coords.latitude;
            this.lang = pos.coords.longitude;
            this.nativeGeocoder.reverseGeocode(this.lat, this.lang)
              .then((result: NativeGeocoderReverseResult[]) => {
                this.lokasi = JSON.stringify(result);
                this.scanner.scan().then(barcodeData => {
                  if (barcodeData.text != "" && barcodeData.text == param.kodecustomer) {
                    var kodeteknisi = JSON.parse(localStorage.getItem('dataTeknisi'))[0].Kode_Karyawan;
                    let datapost = {
                      "kodeteknisi": kodeteknisi,
                      "idwo": param.idwo,
                      "idwd": param.idwd,
                      "lat": JSON.parse(this.lokasi)[0].latitude,
                      "lang": JSON.parse(this.lokasi)[0].longitude,
                      "jalan": JSON.parse(this.lokasi)[0].thoroughfare,
                      "prov": JSON.parse(this.lokasi)[0].administrativeArea,                        "kodepos": JSON.parse(this.lokasi)[0].postalCode
                      
                    }
                    let headers = new Headers();
                    headers.append('Content-Type', 'application/json');
                    this.http.post('http://35.240.151.223:43209/newapimobile/send-close-wo', datapost, {headers: headers}).map(res => res.json()).subscribe(hasil => {
                      var data = hasil['data'];
                      var status = hasil['status'];
                      if (status == true) {
                        this.msg.msg_plainmsg('Berhasil Closed Work Order');
                        this.navCtrl.pop();
                      }else{
                        this.msg.msg_plainmsg('Gagal Closed Work Order, Harap Ulangi');
                      }
                    });
                  }else{
                    this.msg.msg_plainmsg('Uppps... No. Barcode tidak sesuai !!');
                  }
                }).catch(err => {
                  alert(err);
                });
              }).catch((error) => {
                alert("#4Error " + error);
              });
          }).catch((error) => {
            alert("#3Error " + error);
          });
        }).catch((error) => {
          alert("#2Error " + error);
        });
    }).catch((error) => {
      alert("#1Error \n\n" + error);
    });
  }

}
