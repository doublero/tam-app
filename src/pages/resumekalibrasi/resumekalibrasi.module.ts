import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResumekalibrasiPage } from './resumekalibrasi';

@NgModule({
  declarations: [
    ResumekalibrasiPage,
  ],
  imports: [
    IonicPageModule.forChild(ResumekalibrasiPage),
  ],
})
export class ResumekalibrasiPageModule {}
