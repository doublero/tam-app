import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import { MsgProvider } from '../../providers/msg/msg';
import { ForgotPage } from '../forgot/forgot';
import { Storage } from '@ionic/storage';
import { BerandaPage } from '../beranda/beranda';
import { ServiceProvider } from '../../providers/service/service';
import { FindarrProvider } from '../../providers/findarr/findarr';
import { ApiProvider } from '../../providers/api/api';
import {LocalstoragecontrollerProvider} from '../../providers/localstoragecontroller/localstoragecontroller';

@IonicPage()
@Component({
  selector: 'page-logins',
  templateUrl: 'logins.html',
})
export class LoginsPage {
  loading: Loading;
  userdata:any;
  dynamicurl_utama: String;
  apikeyaccess:any;
  menu:any;


  username = '';
  password = '';

  constructor(
    public app: App,
    private api:ApiProvider,
    public service: ServiceProvider,
    private loadingCtrl: LoadingController,
    public navCtrl: NavController,
    private storage: Storage,
    public msg: MsgProvider,
    private localctrl:LocalstoragecontrollerProvider,
    public findarr :FindarrProvider,
    public navParams: NavParams) {
      this.storage.get('dynamicurl_utama').then((val) => {
        this.dynamicurl_utama = val;
      });
      this.storage.get('module_endpoint').then((val) => {
        this.menu = val;
      });
      this.storage.get('userdatateknisi').then((val) => {
        this.userdata = val;
      });
      this.storage.get('apikeyaccess').then((val) => {
        this.apikeyaccess = val;
      });
  }
  dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  
  login(){
      var nextsegment =null;
      if(this.findarr.searchmenu("Login",this.menu) != null){
        nextsegment = this.findarr.searchmenu("Login",this.menu);
        let data = {
          "username": this.username,
          "password": this.password,
          "apikey":this.apikeyaccess
        }
        this.service.postData(data, this.dynamicurl_utama, nextsegment["endpoint"]).then((result) => {
          let data = result["message"];
          if(data == false){
            this.msg.msg_plainmsg(result["data"]);
          }else{
            localStorage.setItem('dataTeknisi', JSON.stringify(result['data'].data_karyawan));
            this.storage.set('userdatateknisi', result["data"]["data_karyawan"]);
            this.storage.set('access', result["data"]["akses"]);
            this.storage.set('datateam', result["data"]["data_team"]);
             this.app.getRootNav().setRoot(BerandaPage); //DO NOT TURN IT OFF WARNIG
          }
        });
      }else{
        this.localctrl.restart();
      }
    }
  openforgotpanel(){
    this.navCtrl.push(ForgotPage);
  }
  
}
