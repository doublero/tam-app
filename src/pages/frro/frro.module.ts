import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FrroPage } from './frro';

@NgModule({
  declarations: [
    FrroPage,
  ],
  imports: [
    IonicPageModule.forChild(FrroPage),
  ],
})
export class FrroPageModule {}
