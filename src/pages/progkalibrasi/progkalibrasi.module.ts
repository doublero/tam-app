import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProgkalibrasiPage } from './progkalibrasi';

@NgModule({
  declarations: [
    ProgkalibrasiPage,
  ],
  imports: [
    IonicPageModule.forChild(ProgkalibrasiPage),
  ],
})
export class ProgkalibrasiPageModule {}
