import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { MsgProvider } from '../../providers/msg/msg';
import { FormkalibrasiPage } from '../formkalibrasi/formkalibrasi';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { FinalkalibrasiPage } from '../finalkalibrasi/finalkalibrasi';

/**
 * Generated class for the ProgkalibrasiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-progkalibrasi',
  templateUrl: 'progkalibrasi.html',
})
export class ProgkalibrasiPage {

  public loading: Loading;
  dataSudahUpload: any;
  dataBelumupload: any;
  jmlSudahUpload: any;
  jmlBelumUpload: any;
  disDone: any;

  lat: any;
  lang: any;
  lokasi: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public http: Http,
    public msg: MsgProvider,
    public locationAccuracy: LocationAccuracy,
    public geo: Geolocation,
    public nativeGeocoder: NativeGeocoder,
    private scanner: BarcodeScanner,
    ) {
  }

  ionViewDidEnter() {
    this.getData();
    if (this.jmlSudahUpload > 0 && this.jmlBelumUpload != 0) {
      this.disDone = true;
    }else if(this.jmlSudahUpload == 0 && this.jmlBelumUpload == 0){
      this.disDone = true;
    }
  }

  btnDone(){
    // alert(this.sudahupload+', '+this.jmlmesin);
    if (this.jmlSudahUpload == 0 && this.jmlBelumUpload != 0 || this.jmlSudahUpload > 0 && this.jmlBelumUpload != 0) {
      this.msg.msg_plainmsg('Belum Menangani Semua Mesin !!');
    }else{
      let postParam = {
        "nowo": this.navParams.get('nowo')
      }
      this.navCtrl.push(FinalkalibrasiPage, postParam);
    }
  }

  getData(){
    this.createLoader('Memuat Data ...');
    this.loading.present().then(() => {
      this.dissmissloading();
      var nowo = this.navParams.get('nowo');
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.get('http://35.240.151.223:43209/apikalibrasi/get-data-kalibrasi-mesin/'+nowo).map(res => res.json()).subscribe(result => {
        var data = result['data'];
        this.dataBelumupload = result['data'];
        this.jmlBelumUpload = data.length;
        console.log(data);
        
      });
      
      this.http.get('http://35.240.151.223:43209/apikalibrasi/get-data-kalibrasi-mesin2/'+nowo).map(res => res.json()).subscribe(result => {
        var data = result['data'];
        this.dataSudahUpload = result['data'];
        this.jmlSudahUpload = data.length;
      });
    });
  }

  refresh(event){
    // this.datamesin = null;
    // this.jmlmesin = null;
    // this.datamesinupload = null;
    // this.sudahupload = null;
    this.jmlSudahUpload = null;
    this.jmlBelumUpload = null;
    this.dataBelumupload = null;
    this.dataSudahUpload = null;
    this.getData();
    setTimeout(() => {
      console.log('berhasil merefresh data');
      event.complete();
    }, 1000);
  }

  createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  scanMesin(param){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.post('http://35.240.151.223:43209/newapimobile/get-scan-header', param, {headers: headers}).map(res => res.json()).subscribe(hasil => {
      var status = hasil['status'];
      var data = hasil['data'];
      if (status == true && data.length > 0 && data.length != null) {
        this.navCtrl.push(FormkalibrasiPage, param);
      } else {
        this.locationAccuracy.canRequest().then((canRequest: boolean) => {
          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
            () => {
              this.geo.getCurrentPosition().then((pos) => {
                this.lat = pos.coords.latitude;
                this.lang = pos.coords.longitude;
                this.nativeGeocoder.reverseGeocode(this.lat, this.lang)
                  .then((result: NativeGeocoderReverseResult[]) => {
                    this.lokasi = JSON.stringify(result);
                    this.scanner.scan().then(barcodeData => {
                      if (barcodeData.text != "" && barcodeData.text == param.nomesin) {
                        let datapost = {
                          "nowo": param.nowo
                        }
                        this.http.post('http://35.240.151.223:43209/newapimobile/get-id-wd', datapost, {headers: headers}).map(res => res.json()).subscribe(hasil => {
                          var data = hasil['data'];
                          var idwd = data[0]['idwd'];
                          let datawd = {
                            "idwd": idwd,
                            "nowo": param.nowo,
                            "noseri": param.nomesin,
                            "jenismesin": 1,
                            "lat": JSON.parse(this.lokasi)[0].latitude,
                            "lang": JSON.parse(this.lokasi)[0].longitude,
                            "jalan": JSON.parse(this.lokasi)[0].thoroughfare,
                            "prov": JSON.parse(this.lokasi)[0].administrativeArea,                        "kodepos": JSON.parse(this.lokasi)[0].postalCode
                          }
                          this.navCtrl.push(FormkalibrasiPage, {'dataparam': datawd});
                        });
                      }else{
                        // alert('Uppps... \n No. Seri mesin tidak sesuai !!');
                        this.msg.msg_plainmsg('No. Seri mesin tidak sesuai !!');
                      }
                    }).catch(err => {
                      alert(err);
                    });
                  }).catch((error) => {
                    alert("#4Error " + error);
                  });
              }).catch((error) => {
                alert("#3Error " + error);
              });
            }).catch((error) => {
              alert("#2Error " + error);
            });
        }).catch((error) => {
          alert("#1Error \n\n" + error);
        });
      }
    });
  }
}
