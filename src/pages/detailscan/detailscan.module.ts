import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailscanPage } from './detailscan';

@NgModule({
  declarations: [
    DetailscanPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailscanPage),
  ],
})
export class DetailscanPageModule {}
