import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KalibrasiPage } from './kalibrasi';

@NgModule({
  declarations: [
    KalibrasiPage,
  ],
  imports: [
    IonicPageModule.forChild(KalibrasiPage),
  ],
})
export class KalibrasiPageModule {}
