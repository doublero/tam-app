import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { MsgProvider } from '../../providers/msg/msg';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { ResumekalibrasiPage } from '../resumekalibrasi/resumekalibrasi';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

/**
 * Generated class for the KalibrasiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-kalibrasi',
  templateUrl: 'kalibrasi.html',
})
export class KalibrasiPage {

  count_not_yet: any;
  count_on_progress: any;
  count_closed: any;
  nama: any;
  nik: any;
  accesstxt: any;
  datawo: any;
  public loading: Loading;
  lokasi: any;
  lat: any;
  lang: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    public msg: MsgProvider,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public locationAccuracy: LocationAccuracy,
    public geo: Geolocation,
    public scanner:BarcodeScanner,
    public nativeGeocoder: NativeGeocoder
    ) {
  }

  ionViewDidEnter() {
    this.getCount();
    this.getData();
  }

  getCount(){
    var kodeteknisi = JSON.parse(localStorage.getItem('dataTeknisi'))[0].Kode_Karyawan;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.get('http://35.240.151.223:43209/apikalibrasi/get-count-kalibrasi/'+kodeteknisi).map(res => res.json()).subscribe(hasil => {
      var status = hasil['status'];
      var data = hasil['data'];
      if (status == true) {
        this.nama = JSON.parse(localStorage.getItem('dataTeknisi'))[0].Nama_Karyawan;
        this.nik = JSON.parse(localStorage.getItem('dataTeknisi'))[0].nik;
        this.count_not_yet = data[0].notyet;
        this.count_on_progress = data[0].progress;
        this.count_closed = data[0].closed;
      }else{
        this.count_not_yet = '0';
        this.count_on_progress = '0';
        this.count_closed = '0';
      }
    });
  }

  getData(){
    this.createLoader('Memuat Data ...');
    this.loading.present().then(() => {
      this.dissmissloading();
      var kodeteknisi = JSON.parse(localStorage.getItem('dataTeknisi'))[0].Kode_Karyawan;
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.get('http://35.240.151.223:43209/apikalibrasi/get-wo-kalibrasi/'+kodeteknisi).map(res => res.json()).subscribe(hasil => {
        var status = hasil['status'];
        var data = hasil['data'];
        if (status == true && data.length > 0 && data != null) {
          this.datawo = data;
        }else{
          this.msg.msg_plainmsg('Data kosong');
        }
      });
    }, (err) => {
      this.dissmissloading();
      this.msg.msg_plainmsg('Gagal Memuat data harap ulangi');
    });
  }

  scanbarcode(datawo) {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        () => {
          this.geo.getCurrentPosition().then((pos) => {
            this.lat = pos.coords.latitude;
            this.lang = pos.coords.longitude;
            this.nativeGeocoder.reverseGeocode(this.lat, this.lang)
              .then((result: NativeGeocoderReverseResult[]) => {
                this.lokasi = JSON.stringify(result);
                this.scanner.scan().then(barcodeData => {
                  if (barcodeData.text != "" && barcodeData.text == datawo.kode_customer) {
                    var kodeteknisi = JSON.parse(localStorage.getItem('dataTeknisi'))[0].Kode_Karyawan;
                        let datapost = {
                          "idwo": datawo.id_wo,
                          "kodeteknisi": kodeteknisi,
                          "lat": JSON.parse(this.lokasi)[0].latitude,
                          "lang": JSON.parse(this.lokasi)[0].longitude,
                          "jalan": JSON.parse(this.lokasi)[0].thoroughfare,
                          "prov": JSON.parse(this.lokasi)[0].administrativeArea,
                          "kodepos": JSON.parse(this.lokasi)[0].postalCode
                        }
                        let headers = new Headers();
                        headers.append('Content-Type', 'application/json');
                        this.http.post('http://35.240.151.223:43209/newapimobile/send-data-wh', datapost, {headers: headers}).map(res => res.json()).subscribe(hasil => {
                          var status = hasil['status'];
                          if (status == true) {
                            this.msg.msg_plainmsg('Berhasil Scan HD Center');
                            this.navCtrl.push(ResumekalibrasiPage, {'idwo': datawo.id_wo, 'nowo': datawo.no_wo});
                          }else{
                            this.msg.msg_plainmsg('Gagal Scan HD Center, Silangkan ulangi');
                          }
                        });
                  }else{
                    this.msg.msg_plainmsg('Gagal Scan HD Center, Silangkan ulangi');
                  }
                }).catch(err => {
                  alert(err);
                });
              }).catch((error) => {
                alert("#4Error " + error);
              });
          }).catch((error) => {
            alert("#3Error " + error);
          });
        }).catch((error) => {
          alert("#2Error " + error);
        });
    }).catch((error) => {
      alert("#1Error \n\n" + error);
    });
  }

  createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  refresh(event){
    this.datawo = null;
    this.nama = null;
    this.nik = null;
    this.accesstxt = null;
    this.count_not_yet = null;
    this.count_on_progress = null;
    this.count_closed = null;
    this.getCount();
    this.getData();
    setTimeout(() => {
      this.nik = JSON.parse(localStorage.getItem('dataTeknisi'))[0].nik;
      this.nama = JSON.parse(localStorage.getItem('dataTeknisi'))[0].Nama_Karyawan;
      console.log('berhasil merefresh data');
      event.complete();
    }, 1000);
  }

  cekStatus1(value) {
    let color = new String;
    if (value == '0') {//0 = UNREAD    ABU-ABU
      color = "onnotyet";
    } else if (value == '1') {//2 = PROGRESS	BIRU
      color = "onproccess";
    } else if (value == '2') {//   3 = CLOSE		  MERAH
      color = "onclose";
    }
    return color;
  }

  cekStatus2(value) {
    let txt = new String;
    if (value == '0') { //0 = blm proses    ABU-ABU
      txt = "NOT YET";
    } else if (value == '1') { //2 = PROGRESS	BIRU
      txt = "ON PROCESS";
    } else if (value == '2') { //   3 = CLOSE		MERAH
      txt = "CLOSED";
    }
    return txt;
  }

  resume(param){
    console.log(param);
    this.navCtrl.push(ResumekalibrasiPage, {'idwo': param.id_wo, 'nowo': param.no_wo});
  }

}
