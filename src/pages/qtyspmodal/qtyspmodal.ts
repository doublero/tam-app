import { Component } from '@angular/core';
import { App,IonicPage, ToastController,NavController,LoadingController, Loading, NavParams,ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MsgProvider } from '../../providers/msg/msg';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';
import { LocalstoragecontrollerProvider } from '../../providers/localstoragecontroller/localstoragecontroller';
import { FindarrProvider } from '../../providers/findarr/findarr';

@IonicPage()
@Component({
  selector: 'page-qtyspmodal',
  templateUrl: 'qtyspmodal.html',
})
export class QtyspmodalPage {
  menus:any;apikeyaccess:any;
  userdata:any;
  public dataedit:any;

  public isReadonly :any = true;
  private formqty: FormGroup;
  loading:Loading;
  no_ref:any;id_dashboard_detail:any;
  constructor(
    private localctrl: LocalstoragecontrollerProvider,
    private app:App,
    private findarr: FindarrProvider,
    private service:ServiceProvider,
    private storage:Storage,
    private views:ViewController,
    private toast:ToastController,
    public navCtrl: NavController,
    private msg:MsgProvider,
    private loadingCtrl: LoadingController,
    private formBuilder: FormBuilder, 
    public navParams: NavParams) {
      this.dataedit = this.navParams.get('data');
      this.no_ref = this.navParams.get('no_ref');
      this.id_dashboard_detail = this.navParams.get('id_dashboard_detail');
      this.validasiinput();
      this.formqty = new FormGroup({
        id_scan_detail_sparepart:new FormControl(),
        qty:new FormControl(),
        id_scan_header:new FormControl(),
        kd_sparepart:new FormControl(),
        id_dashboard_detail:new FormControl(),
        no_ref:new FormControl()
      });
      this.formqty = this.formBuilder.group({
        id_scan_detail_sparepart:  [this.dataedit["id_scan_detail_sparepart"],Validators.required],
        qty:  [this.dataedit["qty"], Validators.required],
        id_scan_header:  [this.dataedit["id_scan_header"], Validators.required],
        kd_sparepart:  [this.dataedit["kd_sparepart"], Validators.required],
        id_dashboard_detail:[this.id_dashboard_detail, Validators.required],
        no_ref:[this.no_ref, Validators.required]
          
      });
      this.storage.get('module_endpoint').then((val) => {
        this.menus = val;
      });
      this.storage.get('userdatateknisi').then((val) => {
        this.userdata = val;
      });
      this.storage.get('apikeyaccess').then((val) => {
        this.apikeyaccess = val;
      });
  }
  validasiinput() {
    this.formqty = this.formBuilder.group({
      id_scan_detail_sparepart:  ['', Validators.required],
      qty:  ['', Validators.required],
      id_scan_header:  ['', Validators.required],
      kd_sparepart:  ['', Validators.required],
      id_dashboard_detail:['', Validators.required],
      no_ref:['', Validators.required]
      
    });
  }
  closeModal(){
    this.views.dismiss();
  }
  simpanform(){
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("UpdateSparepart", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("UpdateSparepart", this.menus);
          let datapost = {
            "data": this.formqty.value,
            "apikey": this.apikeyaccess,
          };
          this.createLoader('Updating Data ...');
          this.loading.present().then(() => {
            this.service.postData(datapost, val, nextsegment["endpoint"]).then((result) => {
              if(result["message"]==true){
                this.toast.create({ message: result["data"], duration: 3000, position: 'top' }).present();
                this.navCtrl.pop();
               }else{
                 this.msg.msg_plainmsg(JSON.stringify(result["data"]));
               }
               this.dissmissloading();
            });
            setTimeout(() => {
              this.dissmissloading();
            }, 3000);
          }, (err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          });
        }else{
          this.localctrl.restart();
        }
      }else{
        this.localctrl.restart();
      }
    });





/*
    this.createLoader('Menyimpan Data ... ');
    this.loading.present().then(() => {
      this.storage.get('dynamicurl_utama').then((val) => {
        if (val != null) {
          let datapost = {
            "data": this.formqty.value,
            "apikey": "660d46013c32ad4998e94f5db2e5048e"
          };
        this.service.postData(datapost, val, "/users/component/sparepart/update").then((result) => {
           if(result["message"]==true){
            this.toast.create({ message: result["data"], duration: 3000, position: 'top' }).present();
            this.navCtrl.pop();
           }else{
             this.msg.msg_plainmsg(JSON.stringify(result["data"]));
           }
           this.dissmissloading();
          });
        }else{
          this.dissmissloading();
          this.storage.clear();
          this.app.getRootNav().setRoot(LoginsPage);
        }
      });
     setTimeout(() => {
        this.dissmissloading();
      }, 5000);
    }, (err) => {
      this.dissmissloading();
      this.msg.msg_plainmsg(err);
    });*/
  }
  public createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  public dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
}
