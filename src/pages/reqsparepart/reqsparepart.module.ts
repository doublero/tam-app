import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReqsparepartPage } from './reqsparepart';

@NgModule({
  declarations: [
    ReqsparepartPage,
  ],
  imports: [
    IonicPageModule.forChild(ReqsparepartPage),
  ],
})
export class ReqsparepartPageModule {}
