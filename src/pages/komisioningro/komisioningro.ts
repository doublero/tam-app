import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import $ from 'jquery';
import 'rxjs/add/operator/map';

/**
 * Generated class for the KomisioningroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-komisioningro',
  templateUrl: 'komisioningro.html',
})
export class KomisioningroPage {

  nowo: any;
  nomesin: any;
  datawd: any;

  // data inputan form
  vessel= "";
  pipingsis = "";
  pompa="";
  listrik="";
  daya="";
  watt="";
  volt="";
  feedpump="";
  filpump="";
  dispump="";
  uv="";
  gpm="";
  tipe="";
  sysconduct="";
  systimer="";
  filsand="";
  filcarb="";
  filresin="";
  filpy="";
  pipingins="";
  syslistrik="";
  syspanel="";
  
  feedpress="";
  syspress="";
  feedflow="";
  permeateflow="";
  permeateconduct="";
  feedconduct="";
  pompafeed="";
  pompafil="";
  pompadis="";
  pompacadfil="";
  pompacaddis="";

  deltapsan="";
  deltapcarbon="";
  deltapresin="";
  deltappyrolox="";

  catvessel= "";
  catpipingsis = "";
  catpompa="";
  catlistrik="";
  catdaya="";
  catfeedpump="";
  catfilpump="";
  catdispump="";
  catuv="";
  catsysconduct="";
  catsystimer="";
  catfilsand="";
  catfilcarb="";
  catfilresin="";
  catfilpy="";
  catpipingins="";
  catsyslistrik="";
  catsyspanel="";
  
  catfeedpress="";
  catsyspress="";
  catfeedflow="";
  catpermeateflow="";
  catpermeateconduct="";
  catfeedconduct="";
  catpompafeed="";
  catpompafil="";
  catpompadis="";
  catpompacadfil="";
  catpompacaddis="";

  satfeedpress="";
  satsyspress="";
  satfeedflow="";
  satpermeateflow="";
  satpermeateconduct="";
  satfeedconduct="";
  satpompafeed="";
  satpompafil="";
  satpompadis="";
  satpompacadfil="";
  satpompacaddis="";
  // data inputan form

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: Http
    ) {
      this.nowo = this.navParams.get('dataparam').nowo;
      this.nomesin = this.navParams.get('dataparam').noseri;
      this.datawd = this.navParams.get('dataparam');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad KomisioningroPage');
  }

  simpanData(){
    
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.post('http://35.240.151.223:43209/newapimobile/send-data-wd', this.datawd, {headers: headers}).map(res => res.json()).subscribe(result => {
      var status = result['status'];
      var data = result['data'];
      var idscan = data['insertId'];
      if (status == true) {
        var date = new Date();
        var bln = date.getMonth()+1;
        var tgl = date.getDate();
        var today = date.getFullYear()+'-'+(bln<10 ? '0' : '')+bln+'-'+(tgl<10 ? '0' : '')+tgl;
        
        let datapost = {
          "id_scan_header": idscan,
          "tgl_instalasi": today,
          "vessel": this.vessel,
          "catvessel":this.catvessel,
          "pipingsis":this.pipingsis,
          "catpipingsis":this.catpipingsis,
          "pompa":this.pompa,
          "catpompa":this.catpompa,
          "listrik":this.listrik,
          "catlistrik":this.catlistrik,
          "daya":this.daya,
          "catdaya":this.catdaya,
          "watt":this.watt,
          "volt":this.volt,
          "feedpump":this.feedpump,
          "catfeedpump":this.catfeedpump,
          "filpump":this.filpump,
          "catfilpump":this.catfilpump,
          "dispump":this.dispump,
          "catdispump":this.catdispump,
          "uv":this.uv,
          "catuv":this.catuv,
          "gpm":this.gpm,
          "tipe":this.tipe,
          "sysconduct":this.sysconduct,
          "catsysconduct":this.catsysconduct,
          "systimer":this.systimer,
          "catsystimer":this.catsystimer,
          "filsand":this.filsand,
          "catfilsand":this.catfilsand,
          "filcarb":this.filcarb,
          "catfilcarb":this.catfilcarb,
          "filresin":this.filresin,
          "catfilresin":this.catfilresin,
          "filpy":this.filpy,
          "catfilpy":this.catfilpy,
          "pipingins":this.pipingins,
          "catpipingins":this.catpipingins,
          "syslistrik":this.syslistrik,
          "catsyslistrik":this.catsyslistrik,
          "syspanel":this.syspanel,
          "catsyspanel":this.catsyspanel,
          
          "feedpress":this.feedpress,
          "syspress":this.syspress,
          "feedflow":this.feedflow,
          "permeateflow":this.permeateflow,
          "permeateconduct":this.permeateconduct,
          "feedconduct":this.feedconduct,
          "pompafeed":this.pompafeed,
          "pompafil":this.pompafil,
          "pompadis":this.pompadis,
          "pompacadfil":this.pompacadfil,
          "pompacaddis":this.pompacaddis,

          "deltapsan":this.deltapsan,
          "deltapcarbon":this.deltapcarbon,
          "deltapresin":this.deltapresin,
          "deltappyrolox":this.deltappyrolox,

          "catfeedpress":this.catfeedpress,
          "catsyspress":this.catsyspress,
          "catfeedflow":this.catfeedflow,
          "catpermeateflow":this.catpermeateflow,
          "catpermeateconduct":this.catpermeateconduct,
          "catfeedconduct":this.catfeedconduct,
          "catpompafeed":this.catpompafeed,
          "catpompafil":this.catpompafil,
          "catpompadis":this.catpompadis,
          "catpompacadfil":this.catpompacadfil,
          "catpompacaddis":this.catpompacaddis,

          "satfeedpress":this.satfeedpress,
          "satsyspress":this.satsyspress,
          "satfeedflow":this.satfeedflow,
          "satpermeateflow":this.satpermeateflow,
          "satpermeateconduct":this.satpermeateconduct,
          "satfeedconduct":this.satfeedconduct,
          "satpompafeed":this.satpompafeed,
          "satpompafil":this.satpompafil,
          "satpompadis":this.satpompadis,
          "satpompacadfil":this.satpompacadfil,
          "satpompacaddis":this.satpompacaddis
        }
        this.http.post('http://35.240.151.223:43209/newapimobile/send-data-instalasiro', datapost, {headers: headers}).map(res => res.json()).subscribe(result => {
          var status = result['status'];
          if (status == true) {
            alert('Berhasil upload data');
            this.navCtrl.pop();
          }else{
            let datacancel = {
              "nowo": this.nowo,
              "noseri": this.nomesin
            }
            this.http.post('http://35.240.151.223:43209/newapimobile/cancel-data-instalasi', datacancel, {headers: headers}).map(res => res.json()).subscribe(result => {
              var status = result['status'];
              if (status == true) {
                alert('Gagal upload data \n Harap isi semua inputan !!');
              }else{
                alert('Gagal cancel data instalasi HD \n Hubungi Dept.MIS');
              }
            });
          }
        });
      }else{
        alert('Gagal upload data scanheader');
      }
    });
  }
}
