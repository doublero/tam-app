import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KomisioningroPage } from './komisioningro';

@NgModule({
  declarations: [
    KomisioningroPage,
  ],
  imports: [
    IonicPageModule.forChild(KomisioningroPage),
  ],
})
export class KomisioningroPageModule {}
