import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinalinstalasiPage } from './finalinstalasi';

@NgModule({
  declarations: [
    FinalinstalasiPage,
  ],
  imports: [
    IonicPageModule.forChild(FinalinstalasiPage),
  ],
})
export class FinalinstalasiPageModule {}
