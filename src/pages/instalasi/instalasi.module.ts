import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InstalasiPage } from './instalasi';

@NgModule({
  declarations: [
    InstalasiPage,
  ],
  imports: [
    IonicPageModule.forChild(InstalasiPage),
  ],
})
export class InstalasiPageModule {}
