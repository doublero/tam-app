import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import $ from 'jquery';
import { Storage } from '@ionic/storage';
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { ResumeinstalasiPage } from '../resumeinstalasi/resumeinstalasi';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { MsgProvider } from '../../providers/msg/msg';
/**
 * Generated class for the FinstalasihdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-instalasi',
  templateUrl: 'instalasi.html',
})
export class InstalasiPage {

  count_not_yet: any;
  count_on_progress: any;
  count_closed: any;
  nama: any;
  nik: any;
  accesstxt: any;

  konmes = '';
  keterangan = '';
  id_scan_header: any;
  loading: Loading;
  myWO: any;
  lokasi: any;
  lat: any;
  lang: any;
  idwh: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private locationAccuracy: LocationAccuracy,
    public storage: Storage,
    public http: Http,
    public geo: Geolocation,
    public nativeGeocoder: NativeGeocoder,
    private scanner: BarcodeScanner,
    public msg: MsgProvider
  ) {
    // this.getData();
  }

  public createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  public dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  ionViewDidEnter() {
    var date = new Date();
    var bln = date.getMonth()+1;
    var tgl = date.getDate();
    var today = date.getFullYear()+'-'+(bln<10 ? '0' : '')+bln+'-'+(tgl<10 ? '0' : '')+tgl;
    $('#today').text(': '+today);
    this.getData();
    // this.myWO = JSON.parse(localStorage.getItem('datawoinstalasi'));
    this.nama = JSON.parse(localStorage.getItem('dataTeknisi'))[0].Nama_Karyawan;
    this.nik = JSON.parse(localStorage.getItem('dataTeknisi'))[0].nik;
    this.getCountInstalasi();
  }

  getData(){
    this.createLoader('Memuat Data ...');
    this.loading.present().then(() => {
      this.dissmissloading();
      var kodeteknisi = JSON.parse(localStorage.getItem('dataTeknisi'))[0].Kode_Karyawan;
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.get('http://35.240.151.223:43209/apiwebteknisi/get-data-wo-instalasi/'+kodeteknisi).map(res => res.json()).subscribe(hasil => {
        var status = hasil['status'];
        var data = hasil['data'];
        if (status == true && data.length > 0 && data != null) {
          this.myWO = data;
        }else{
          this.msg.msg_plainmsg('Data kosong');
        }
      });
    }, (err) => {
      this.dissmissloading();
      this.msg.msg_plainmsg('Gagal Memuat data harap ulangi');
    });
    // $.ajax({
    //   url: "http://35.240.151.223:43209/apiwebteknisi/get-data-wo-instalasi/"+kodeTeknisi,
    //   type: "get",
    //   dataType: "json",
    //   success: function (result) {
    //     var data = result.data;
    //     localStorage.removeItem('datawoinstalasi');
    //     localStorage.setItem('datawoinstalasi', JSON.stringify(data));
    //     console.log(kodeTeknisi);
    //   }
    // });
  }

  refresh(event){
    this.myWO = null;
    this.nama = null;
    this.nik = null;
    this.accesstxt = null;
    this.count_not_yet = null;
    this.count_on_progress = null;
    this.count_closed = null;
    this.getCountInstalasi();
    this.getData();
    setTimeout(() => {
      // this.myWO = JSON.parse(localStorage.getItem('datawoinstalasi'));
      this.nik = JSON.parse(localStorage.getItem('dataTeknisi'))[0].nik;
      this.nama = JSON.parse(localStorage.getItem('dataTeknisi'))[0].Nama_Karyawan;
      console.log('berhasil merefresh data');
      event.complete();
    }, 1000);
  }

  cekStatus1(value) {
    let color = new String;
    if (value == '0') {//0 = UNREAD    ABU-ABU
      color = "onnotyet";
    } else if (value == '1') {//2 = PROGRESS	BIRU
      color = "onproccess";
    } else if (value == '2') {//   3 = CLOSE		  MERAH
      color = "onclose";
    }
    return color;
  }

  cekStatus2(value) {
    let txt = new String;
    if (value == '0') { //0 = blm proses    ABU-ABU
      txt = "NOT YET";
    } else if (value == '1') { //2 = PROGRESS	BIRU
      txt = "ON PROCESS";
    } else if (value == '2') { //   3 = CLOSE		MERAH
      txt = "CLOSED";
    }
    return txt;
  }

  scanbarcode(datawo) {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        () => {
          this.geo.getCurrentPosition().then((pos) => {
            this.lat = pos.coords.latitude;
            this.lang = pos.coords.longitude;
            this.nativeGeocoder.reverseGeocode(this.lat, this.lang)
              .then((result: NativeGeocoderReverseResult[]) => {
                this.lokasi = JSON.stringify(result);
                this.scanner.scan().then(barcodeData => {
                  if (barcodeData.text != "" && barcodeData.text == datawo.kode_customer) {
                    var kodeteknisi = JSON.parse(localStorage.getItem('dataTeknisi'))[0].Kode_Karyawan;
                        let datapost = {
                          "idwo": datawo.id_wo,
                          "kodeteknisi": kodeteknisi,
                          "lat": JSON.parse(this.lokasi)[0].latitude,
                          "lang": JSON.parse(this.lokasi)[0].longitude,
                          "jalan": JSON.parse(this.lokasi)[0].thoroughfare,
                          "prov": JSON.parse(this.lokasi)[0].administrativeArea,
                          "kodepos": JSON.parse(this.lokasi)[0].postalCode
                        }
                        let headers = new Headers();
                        headers.append('Content-Type', 'application/json');
                        this.http.post('http://35.240.151.223:43209/newapimobile/send-data-wh', datapost, {headers: headers}).map(res => res.json()).subscribe(hasil => {
                          var status = hasil['status'];
                          if (status == true) {
                            this.msg.msg_plainmsg('Berhasil Scan HD Center');
                            this.navCtrl.push(ResumeinstalasiPage, {"nowo": datawo.no_wo });
                          }else{
                            this.msg.msg_plainmsg('Gagal Scan HD Center, Silangkan ulangi');
                          }
                        });
                  }else{
                    this.msg.msg_plainmsg('Gagal Scan HD Center, Silangkan ulangi');
                  }
                }).catch(err => {
                  alert(err);
                });
              }).catch((error) => {
                alert("#4Error " + error);
              });
          }).catch((error) => {
            alert("#3Error " + error);
          });
        }).catch((error) => {
          alert("#2Error " + error);
        });
    }).catch((error) => {
      alert("#1Error \n\n" + error);
    });
  }

  resume(item){
    this.navCtrl.push(ResumeinstalasiPage, {"nowo": item.no_wo });
  }

  getCountInstalasi(){
    let datapost = {
      "kodeteknisi": JSON.parse(localStorage.getItem('dataTeknisi'))[0].Kode_Karyawan
    }
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.post('http://35.240.151.223:43209/newapimobile/get-count-instalasi', datapost, {headers: headers}).map(res => res.json()).subscribe(hasil => {
      var status = hasil['status'];
      var data = hasil['data'];
      if (status == true) {
        this.count_not_yet = data[0].notyet;
        this.count_on_progress = data[0].progress;
        this.count_closed = data[0].closed;
      }else{
        this.count_not_yet = '0';
        this.count_on_progress = '0';
        this.count_closed = '0';
      }
    });
  }

}
