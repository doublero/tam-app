import { Component, ViewChild } from '@angular/core';
import { App,NavController, NavParams, normalizeURL, LoadingController, Loading } from 'ionic-angular';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';
import { File, IWriteOptions } from '@ionic-native/file';
import { MsgProvider } from '../../providers/msg/msg';
import { ServiceProvider } from '../../providers/service/service';
import { ApiProvider } from '../../providers/api/api';
import { LocalstoragecontrollerProvider } from '../../providers/localstoragecontroller/localstoragecontroller';
import { FindarrProvider } from '../../providers/findarr/findarr';



const STORAGE_KEY = 'IMAGE_LIST';
@Component({
  selector: 'page-ttdwo',
  templateUrl: 'ttdwo.html',
})
export class TtdwoPage {
  public datawo:any;
public datateknisi:any;

  signature = '';
  menus:any;userdata:any;apikeyaccess:any;
  isDrawing = false;
  id_wo:any;
  storedImages = [];
  no_wo: any; id_teknisi: any;
  loading: Loading;
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  private signaturePadOptions: Object = { // Check out https://github.com/szimek/signature_pad
    'minWidth': 2,
    'canvasWidth': 400,
    'canvasHeight': 200,
    'backgroundColor': '#f6fbff',
    'penColor': '#666a73',
    'borderColor': '#666a73'
  };
  constructor(
    private app:App,
    private api:ApiProvider,
    public navController: NavController,
    public storage: Storage,
    private localctrl: LocalstoragecontrollerProvider,
    private findarr: FindarrProvider,
    public toastCtrl: ToastController,
    private file: File,
    private service: ServiceProvider,
    private loadingCtrl: LoadingController,
    private msg: MsgProvider,
    private navParams: NavParams
    ) {
     this.datateknisi = this.navParams.get('datateknisi'); 
     this.datawo =  this.navParams.get('datawo');
     this.no_wo = this.datawo["no_wo"]; 
     this.storage.get('module_endpoint').then((val) => {
      this.menus = val;
    });
    this.storage.get('apikeyaccess').then((val) => {
      this.apikeyaccess = val;
    });
    
  }

  ionViewDidEnter() {
    this.signaturePad.clear()
    this.storage.get('savedSignature').then((data) => {
      this.signature = data;
    });
  }
  drawComplete() {
    this.isDrawing = false;
  }
  drawStart() {
    this.isDrawing = true;
  }
  clearPad() {
    this.signaturePad.clear();
  }
  public createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  public dissmissloading(){
    if(this.loading){
        this.loading.dismiss();
        this.loading = null;
    }
  }
  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  removeImageAtIndex(index) {
    let removed = this.storedImages.splice(index, 1);
    this.file.removeFile(this.file.dataDirectory, removed[0].img).then(res => {
    }, err => {
      console.log('remove err; ', err);
    });
    this.storage.set(STORAGE_KEY, this.storedImages);
  }
  savePad() {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("Upload64img", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("Upload64img", this.menus);
          var dataUrl = this.signaturePad.toDataURL();
          let datapost = {
            "data": dataUrl,
            "datawo": this.datawo,
            "datateknisi": this.datateknisi,
            "apikey": this.apikeyaccess
          };
          this.createLoader('Posting Data ...');
          this.loading.present().then(() => {
            this.service.postData(datapost, val, nextsegment["endpoint"]).then((result) => {
              this.presentToast(result["data"]);
            });
           setTimeout(() => {
              this.dissmissloading();
            }, 5000);
          }, (err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          });
        }else{
          this.localctrl.restart();
        }
      }else{
        this.localctrl.restart();
      }
    });
  }
  /*savePad() {
    this.createLoader('Loading ...');
    this.loading.present().then(() => {
      var dataUrl = this.signaturePad.toDataURL();
      let datapost = {
        "data": dataUrl,
        "datawo": this.datawo,
        "datateknisi": this.datateknisi,
        "apikey": "660d46013c32ad4998e94f5db2e5048e"
      };
      this.storage.get('dynamicurl_utama').then((val) => {
        if (val != null) {
          this.service.postData(datapost, val, "/users/wo/uploadphoto64").then((result) => {
          this.dissmissloading();
            this.presentToast(result['data']);
            this.navController.pop();
          });
        }
      });
      setTimeout(() => {
        this.dissmissloading();
      }, 5000);
    }, (err) => {
      this.dissmissloading();
      this.msg.msg_plainmsg(err);
    });
  }*/
}
