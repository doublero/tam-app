import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormkalibrasiPage } from './formkalibrasi';

@NgModule({
  declarations: [
    FormkalibrasiPage,
  ],
  imports: [
    IonicPageModule.forChild(FormkalibrasiPage),
  ],
})
export class FormkalibrasiPageModule {}
