import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { MsgProvider } from '../../providers/msg/msg';

/**
 * Generated class for the FormkalibrasiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-formkalibrasi',
  templateUrl: 'formkalibrasi.html',
})
export class FormkalibrasiPage {

  nowo: any;
  noseri: any;
  datawd: any;

  ns = "";
  kns = "";
  apo = "";
  kapo = "";
  apc = "";
  kapc = "";
  vpo = "";
  kvpo = "";
  vpc = "";
  kvpc = "";
  dpo = "";
  kdpo = "";
  dpc = "";
  kdpc = "";
  qd3 = "";
  kqd3 = "";
  qd5 = "";
  kqd5 = "";
  qd8 = "";
  kqd8 = "";
  qb1 = "";
  kqb1 = "";
  qb2 = "";
  kqb2 = "";
  qb3 = "";
  kqb3 = "";
  lt = "";
  klt = "";
  ht = "";
  kht = "";
  rc = "";
  krc = "";
  hp = "";
  khp = "";
  unit = "";
  model = "";
  catatan = "";
  catatan2 = "";


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    public msg: MsgProvider
    ) {
  }

  ionViewDidEnter() {
    this.nowo = this.navParams.get('dataparam').nowo;
    this.noseri = this.navParams.get('dataparam').noseri;
    this.datawd = this.navParams.get('dataparam');
  }

  simpanData(){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.post('http://35.240.151.223:43209/apikalibrasi/send-data-wd', this.datawd, {headers: headers}).map(res => res.json()).subscribe(result => {
      var status = result['status'];
      var data = result['data'];
      var idscan = data['insertId'];
      if (status == true) {
        var date = new Date();
        var bln = date.getMonth()+1;
        var tgl = date.getDate();
        var today = date.getFullYear()+'-'+(bln<10 ? '0' : '')+bln+'-'+(tgl<10 ? '0' : '')+tgl;

        let datapost = {
          'id_scan_header': idscan,
          'tgl_kalibrasi': today,
          "ns": this.ns,
          "apo": this.apo,
          "kapo": this.kapo,
          "apc": this.apc,
          "kapc":this.kapc,
          "vpo": this.vpo,
          "kvpo": this.kvpo,
          "vpc": this.vpc,
          "kvpc": this.kvpc,
          "dpo": this.dpo,
          "kdpo": this.kdpo,
          "dpc": this.dpc,
          "kdpc": this.kdpc,
          "qd3": this.qd3,
          "kqd3": this.kqd3,
          "qd5": this.qd5,
          "kqd5": this.kqd5,
          "qd8": this.qd8,
          "kqd8": this.kqd8,
          "qb1": this.qb1,
          "kqb1": this.kqb1,
          "qb2": this.qb2,
          "kqb2": this.kqb2,
          "qb3": this.qb3,
          "kqb3": this.kqb3,
          "lt": this.lt,
          "klt": this.klt,
          "ht": this.ht,
          "kht": this.kht,
          "rc": this.rc,
          "krc": this.krc,
          "hp": this.hp,
          "khp": this.khp,
          "unit": this.unit,
          "model": this.model,
          "catatan": this.catatan,
          "catatan2": this.catatan2
        }
        this.http.post('http://35.240.151.223:43209/apikalibrasi/send-data-kalibrasi', datapost, {headers: headers}).map(res => res.json()).subscribe(result => {
          var status = result['status'];
          if (status == true) {
            this.msg.msg_plainmsg('Berhasil upload data');
            this.navCtrl.pop();
          }else{
            let datacancel = {
              "nowo": this.nowo,
              "noseri": this.noseri,
            }
            this.http.post('http://35.240.151.223:43209/apikalibrasi/cancel-data-kalibrasi', datacancel, {headers: headers}).map(res => res.json()).subscribe(result => {
              var status = result['status'];
              if (status == true) {
                this.msg.msg_plainmsg('Gagal upload data, Harap cek kembali isian form');
              }else{
                this.msg.msg_plainmsg('Gagal cancel data kalibrasi, Hubungi Dept.MIS');
              }
            });
          }
        });
      }
    });
  }

}
