import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { File } from '@ionic-native/file';
import { FindarrProvider } from '../../providers/findarr/findarr';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { MsgProvider } from '../../providers/msg/msg';

/**
 * Generated class for the FinalkalibrasiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-finalkalibrasi',
  templateUrl: 'finalkalibrasi.html',
})
export class FinalkalibrasiPage {

  nama="";
  jabatan="";

  signature = '';
  nowo: any;
  isDrawing: any;
  menus:any;
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  private signaturePadOptions: Object = { // Check out https://github.com/szimek/signature_pad
    'minWidth': 1,
    'canvasWidth': 350,
    'canvasHeight': 200,
    'backgroundColor': '#FFFFFF',
    'penColor': '#666a73'
  };
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: Http,
    public findarr: FindarrProvider,
    public msg: MsgProvider
  ) {
  }

  ionViewDidEnter() {
    this.nowo = this.navParams.get('nowo');
  }

  drawComplete() {
    this.isDrawing = false;
  }
  drawStart() {
    this.isDrawing = true;
  }
  clearPad() {
    this.signaturePad.clear();
  }
  savePad(){
    var a = this.nama.trim();
    var b = this.jabatan.trim();
    if (a == "" || b == "") {
      this.msg.msg_plainmsg("Tidak dapat menyimpan !! \n Harap isi nama dan jabatan terlebih dahulu..");
    }else if(this.signaturePad.isEmpty()){
      this.msg.msg_plainmsg("Tidak dapat menyimpan !! \n Harap tanda tangan terlebih dahulu..");
    }else{
      let datapost = {
        "nowo": this.nowo,
        "nama": a,
        "jabatan": b,
        "datattd": this.signaturePad.toDataURL()
      }
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post('http://35.240.151.223:43209/newapimobile/send-ttd-instalasi', datapost, {headers: headers}).map(res => res.json()).subscribe(result => {
        var status = result['status'];
        if (status == true) {
          this.msg.msg_plainmsg('Berhasil Upload tanda tangan');
          this.navCtrl.pop();
        }else{
          this.msg.msg_plainmsg('Gagal Upload tanda tangan');
        }
      });
    }
  }

}
