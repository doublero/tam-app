import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinalkalibrasiPage } from './finalkalibrasi';

@NgModule({
  declarations: [
    FinalkalibrasiPage,
  ],
  imports: [
    IonicPageModule.forChild(FinalkalibrasiPage),
  ],
})
export class FinalkalibrasiPageModule {}
