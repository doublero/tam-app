import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import $ from 'jquery';
import { ProginstalasiPage } from '../proginstalasi/proginstalasi';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { MsgProvider } from '../../providers/msg/msg';

/**
 * Generated class for the ResumeinstalasiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resumeinstalasi',
  templateUrl: 'resumeinstalasi.html',
})
export class ResumeinstalasiPage {

  nowo: any;
  datawo: any;
  lat: any;
  lang: any;
  lokasi: any;
  public loading: Loading;

  id_wo : any;
  no_wo: any;
  id_wd: any;
  STATUS: any;
  STATUS_instalasi: any;
  closed_by: any;
  closed_time: any;
  open_by: any;
  open_time: any;
  kode_customer: any;
  tglwo : any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private locationAccuracy: LocationAccuracy,
    public geo: Geolocation,
    private http: Http,
    public nativeGeocoder: NativeGeocoder,
    private scanner: BarcodeScanner,
    public loadingCtrl: LoadingController,
    public msg: MsgProvider

    ) {
      // this.nowo = this.navParams.get('nowo');
      // this.getData();
      // this.datawo = JSON.parse(localStorage.getItem('detailwo'))[0];
  }

  ionViewDidEnter() {
    this.getData();
    // this.datawo = JSON.parse(localStorage.getItem('detailwo'))[0];
    console.log(this.datawo);
    
  }

  getData(){
    this.createLoader('Memuat Data ...');
    this.loading.present().then(() => {
      this.dissmissloading();
      var nowo = this.navParams.get('nowo');
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.get('http://35.240.151.223:43209/newapimobile/get-data-detail-wo/'+nowo).map(res => res.json()).subscribe(hasil => {
        var status = hasil['status'];
        var data = hasil['data'];
        if (status == true && data.length > 0 && data != null) {
          this.id_wo = data[0].id_wo;
          this.no_wo= data[0].no_wo;
          this.id_wd= data[0].id_wd;
          this.STATUS= data[0].STATUS;
          this.STATUS_instalasi= data[0].STATUS_instalasi;
          this.closed_by= data[0].closed_by;
          this.closed_time= data[0].closed_time;
          this.open_by= data[0].open_by;
          this.open_time= data[0].open_time;
          this.kode_customer= data[0].kode_customer;
          this.tglwo = data[0].datecreated;
        }else{
          this.msg.msg_plainmsg('Data kosong');
        }
      });
    }, (err) => {
      this.dissmissloading();
      this.msg.msg_plainmsg('Gagal Memuat data harap ulangi');
    });
    // $.ajax({
    //   url: "http://35.240.151.223:43209/newapimobile/get-data-detail-wo/"+nowo,
    //   type: "get",
    //   dataType: "json",
    //   success: function (result) {
    //     var pesan = result.pesan;
    //     var data = result.data;
    //     localStorage.removeItem('detailwo');
    //     localStorage.setItem('detailwo', JSON.stringify(data));
    //   }
    // });
  }

  cekStatus1(value) {
    let color = new String;
    if (value == '0') {//0 = UNREAD    ABU-ABU
      color = "onnotyet";
    } else if (value == '1') {//2 = PROGRESS	BIRU
      color = "onproccess";
    } else if (value == '2') {//   3 = CLOSE		  MERAH
      color = "onclose";
    }
    return color;
  }

  cekStatus2(value) {
    let txt = new String;
    if (value == '0') { //0 = blm proses    ABU-ABU
      txt = "NOT YET";
    } else if (value == '1') { //2 = PROGRESS	BIRU
      txt = "ON PROCESS";
    } else if (value == '2') { //   3 = CLOSE		MERAH
      txt = "CLOSED";
    }
    return txt;
  }

  progressInstalasi(params){
    var nowo = params.nowo;
    this.navCtrl.push(ProginstalasiPage, {"nowo": nowo });
  }

  refresh(event){
    this.id_wo = null;
    this.no_wo= null;
    this.id_wd= null;
    this.STATUS= null;
    this.STATUS_instalasi= null;
    this.closed_by= null;
    this.closed_time= null;
    this.open_by= null;
    this.open_time= null;
    this.kode_customer= null;
    this.tglwo = null;
    this.getData();
    setTimeout(() => {
      // this.datawo = JSON.parse(localStorage.getItem('detailwo'))[0];
      console.log('berhasil merefresh data');
      // console.log(this.datawo);
      event.complete();
    }, 1000);
  }

  btnstatus(param){
    let status = new Boolean;
    if (param == '1' && param != null && param != "") {
      status = false;
    } else {
      status = true;
    }
    return status;
  }

  btnprogress(param){
    let status = new Boolean;
    if (param == '1' && param != null && param != "") {
      status = true;
    } else {
      status = false;
    }
    return status;
  }

  closedwo(param){
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        () => {
          this.geo.getCurrentPosition().then((pos) => {
            this.lat = pos.coords.latitude;
            this.lang = pos.coords.longitude;
            this.nativeGeocoder.reverseGeocode(this.lat, this.lang)
              .then((result: NativeGeocoderReverseResult[]) => {
                this.lokasi = JSON.stringify(result);
                this.scanner.scan().then(barcodeData => {
                  if (barcodeData.text != "" && barcodeData.text == param.kodecustomer) {
                    var kodeteknisi = JSON.parse(localStorage.getItem('dataTeknisi'))[0].Kode_Karyawan;
                    let datapost = {
                      "kodeteknisi": kodeteknisi,
                      "idwo": param.idwo,
                      "idwd": param.idwd,
                      "lat": JSON.parse(this.lokasi)[0].latitude,
                      "lang": JSON.parse(this.lokasi)[0].longitude,
                      "jalan": JSON.parse(this.lokasi)[0].thoroughfare,
                      "prov": JSON.parse(this.lokasi)[0].administrativeArea,                        "kodepos": JSON.parse(this.lokasi)[0].postalCode
                      
                    }
                    let headers = new Headers();
                    headers.append('Content-Type', 'application/json');
                    this.http.post('http://35.240.151.223:43209/newapimobile/send-close-wo', datapost, {headers: headers}).map(res => res.json()).subscribe(hasil => {
                      var data = hasil['data'];
                      var status = hasil['status'];
                      if (status == true) {
                        this.msg.msg_plainmsg('Berhasil Closed Work Order');
                        this.navCtrl.pop();
                      }else{
                        this.msg.msg_plainmsg('Gagal Closed Work Order, Harap Ulangi');
                      }
                    });
                  }else{
                    this.msg.msg_plainmsg('Uppps... No. Barcode tidak sesuai !!');
                  }
                }).catch(err => {
                  alert(err);
                });
              }).catch((error) => {
                alert("#4Error " + error);
              });
          }).catch((error) => {
            alert("#3Error " + error);
          });
        }).catch((error) => {
          alert("#2Error " + error);
        });
    }).catch((error) => {
      alert("#1Error \n\n" + error);
    });
  }

  createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

}
