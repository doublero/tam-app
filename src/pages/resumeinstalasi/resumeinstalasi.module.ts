import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResumeinstalasiPage } from './resumeinstalasi';

@NgModule({
  declarations: [
    ResumeinstalasiPage,
  ],
  imports: [
    IonicPageModule.forChild(ResumeinstalasiPage),
  ],
})
export class ResumeinstalasiPageModule {}
