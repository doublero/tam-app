import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import $ from 'jquery';
import 'rxjs/add/operator/map';

/**
 * Generated class for the KomisioninghdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-komisioninghd',
  templateUrl: 'komisioninghd.html',
})
export class KomisioninghdPage {

  nowo: any;
  nomesin: any;
  datawd: any;

  //data inputan form
  typeconcent1 = "";
  settrange1="";
  concent1="";
  baseconduct1="";
  baseb1="";
  basealarm1="";
  typebci1="";
  b1="";
  mixratio1="";
  mixingwater1="";
  tmpalarmmax1="";
  tmpalarmmin1="";
  conalarmmax1="";
  conalarmmin1="";
  bconalarmmax1="";
  bconalarmmin1="";
  aceticorigin1="";
  citricorigin1="";

  typeconcent2 = "";
  settrange2="";
  concent2="";
  baseconduct2="";
  baseb2="";
  basealarm2="";
  typebci2="";
  b2="";
  mixratio2="";
  mixingwater2="";
  tmpalarmmax2="";
  tmpalarmmin2="";
  conalarmmax2="";
  conalarmmin2="";
  bconalarmmax2="";
  bconalarmmin2="";
  aceticorigin2="";
  citricorigin2="";
  //data inputan form


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: Http
  ) {
    this.nowo = this.navParams.get('dataparam').nowo;
    this.nomesin = this.navParams.get('dataparam').noseri;
    this.datawd = this.navParams.get('dataparam');
  }

  ionViewDidLoad() {
    
  }

  simpanData(){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.post('http://35.240.151.223:43209/newapimobile/send-data-wd', this.datawd, {headers: headers}).map(res => res.json()).subscribe(result => {
      var status = result['status'];
      var data = result['data'];
      var idscan = data['insertId'];
      if (status == true) {
        var date = new Date();
        var bln = date.getMonth()+1;
        var tgl = date.getDate();
        var today = date.getFullYear()+'-'+(bln<10 ? '0' : '')+bln+'-'+(tgl<10 ? '0' : '')+tgl;
        
        let datapost = {
          'id_scan_header': idscan,
          'tgl_instalasi': today,
          'typeconcent1': this.typeconcent1,
          'settrange1': this.settrange1,
          'concent1': this.concent1,
          'baseconduct1': this.baseconduct1,
          'baseb1': this.baseb1,
          'basealarm1': this.basealarm1,
          'typebci1': this.typebci1,
          'b1': this.b1,
          'mixratio1': this.mixratio1,
          'mixingwater1': this.mixingwater1,
          'tmpalarmmax1': this.tmpalarmmax1,
          'tmpalarmmin1': this.tmpalarmmin1,
          'conalarmmax1': this.conalarmmax1,
          'conalarmmin1': this.conalarmmin1,
          'bconalarmmax1': this.bconalarmmax1,
          'bconalarmmin1': this.bconalarmmin1,
          'aceticorigin1': this.aceticorigin1,
          'citricorigin1': this.citricorigin1,
  
          'typeconcent2': this.typeconcent2,
          'settrange2': this.settrange2,
          'concent2': this.concent2,
          'baseconduct2': this.baseconduct2,
          'baseb2': this.baseb2,
          'basealarm2': this.basealarm2,
          'typebci2': this.typebci2,
          'b2': this.b2,
          'mixratio2': this.mixratio2,
          'mixingwater2': this.mixingwater2,
          'tmpalarmmax2': this.tmpalarmmax2,
          'tmpalarmmin2': this.tmpalarmmin2,
          'conalarmmax2': this.conalarmmax2,
          'conalarmmin2': this.conalarmmin2,
          'bconalarmmax2': this.bconalarmmax2,
          'bconalarmmin2': this.bconalarmmin2,
          'aceticorigin2': this.aceticorigin2,
          'citricorigin2': this.citricorigin2
        }
        this.http.post('http://35.240.151.223:43209/newapimobile/send-data-instalasihd', datapost, {headers: headers}).map(res => res.json()).subscribe(result => {
          var status = result['status'];
          if (status == true) {
            alert('Berhasil upload data');
            this.navCtrl.pop();
          }else{
            let datacancel = {
              "nowo": this.nowo,
              "noseri": this.nomesin,
            }
            this.http.post('http://35.240.151.223:43209/newapimobile/cancel-data-instalasi', datacancel, {headers: headers}).map(res => res.json()).subscribe(result => {
              var status = result['status'];
              if (status == true) {
                alert('Gagal upload data');
              }else{
                alert('Gagal cancel data instalasi HD \n Hubungi Dept.MIS');
              }
            });
          }
        });
      }else{
        alert('Gagal upload data scanheader');
      }
    });
  }

}
