import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KomisioninghdPage } from './komisioninghd';

@NgModule({
  declarations: [
    KomisioninghdPage,
  ],
  imports: [
    IonicPageModule.forChild(KomisioninghdPage),
  ],
})
export class KomisioninghdPageModule {}
