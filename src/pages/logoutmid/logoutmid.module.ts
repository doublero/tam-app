import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogoutmidPage } from './logoutmid';

@NgModule({
  declarations: [
    LogoutmidPage,
  ],
  imports: [
    IonicPageModule.forChild(LogoutmidPage),
  ],
})
export class LogoutmidPageModule {}
