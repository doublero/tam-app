import { Component } from '@angular/core';
import { App,IonicPage, NavController,Platform,ActionSheetController, NavParams,LoadingController, Loading  } from 'ionic-angular';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { LocalstoragecontrollerProvider } from '../../providers/localstoragecontroller/localstoragecontroller';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Storage } from '@ionic/storage';
import { FindarrProvider } from '../../providers/findarr/findarr';
import { ServiceProvider } from '../../providers/service/service';
import { MsgProvider } from '../../providers/msg/msg';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import {UploadPage} from '../upload/upload';
import { ToastController } from 'ionic-angular';
import {BerandaPage} from '../beranda/beranda';



@IonicPage()
@Component({
  selector: 'page-logoutmid',
  templateUrl: 'logoutmid.html',
})
export class LogoutmidPage {
  apikeyaccess: any;
  loading: Loading;
  lat:any;
  lng:any;lokasi:any;
  userdata:any;id_karyawan:any;menus: any;
  id_dashboardetail:any;dataparam:any;data_wo:any;
  constructor(
    private scanner:BarcodeScanner,
    private nativeGeocoder:NativeGeocoder,
    private geo:Geolocation,
    private locationAccuracy:LocationAccuracy,
    public navCtrl: NavController, 
    private localctrl:LocalstoragecontrollerProvider,
    public navParams: NavParams,
    private storage:Storage,
    private msg:MsgProvider,
    private camera:Camera,
    private loadingCtrl: LoadingController,
    private service:ServiceProvider,
    public actionSheetCtrl: ActionSheetController, 
    private platform: Platform, 
    private filePath:FilePath,
    private toast:ToastController,
    private app:App,
    private findarr: FindarrProvider) {
      this.id_dashboardetail = this.navParams.get('id_dashboardetail');
   this.dataparam = this.navParams.get('dataparam');
   this.data_wo = this.navParams.get('data_wo');
   
      this.storage.get('apikeyaccess').then((val) => {
        this.apikeyaccess = val;
      });
      this.storage.get('userdatateknisi').then((val) => {
        this.userdata = val[0];
        this.id_karyawan = val[0].Kode_Karyawan
      });
      this.storage.get('module_endpoint').then((val) => {
        this.menus = val;
      });
  }

  logoutfunc(databarcpde, lng, lat, lokasi, datatiket){
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("LogoutTeam", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("LogoutTeam", this.menus);
          var datapostcount = {
            "apikey": this.apikeyaccess,
            "barcode":databarcpde,
            "lng":lng,
            "lat":lat,
            "lokasi":lokasi,
            "datatiket":this.data_wo,
            "id_dashboard_detail":datatiket
          }
          this.createLoader('Posting Data ...');
          this.loading.present().then(() => {
            this.service.postData(datapostcount, val,nextsegment["endpoint"]).then((result) => {
              this.dissmissloading();
              if(result["message"]==true){
                this.toast.create({ message: result["data"], duration: 3000, position: 'top' }).present();
                this.app.getRootNav().setRoot(BerandaPage);  
              }else{
                this.msg.msg_plainmsg(result["data"]);
              }
               });
            setTimeout(() => {
              this.dissmissloading();
            }, 3000);
          }, (err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          });
          /**/
        }
      }else {
        this.localctrl.restart();
      }
    });
  }
  public createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  scanbarcode() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        () => {
          this.geo.getCurrentPosition().then((pos) => {
            this.lat = pos.coords.latitude;
            this.lng = pos.coords.longitude;
            this.nativeGeocoder.reverseGeocode(this.lat, this.lng)
              .then((result: NativeGeocoderReverseResult[]) => {
                  this.lokasi = JSON.stringify(result);
                this.scanner.scan().then(barcodeData => {
                  if (barcodeData.text != "") {
                     this.logoutfunc(barcodeData.text, this.lng, this.lat, this.lokasi,this.id_dashboardetail ); 
                  }
                }).catch(err => {
                  this.msg.msg_plainmsg(err);
                });
              }).catch((error) => {
                this.msg.msg_plainmsg("#4Error " + error);
              });
          }).catch((error) => {
            this.msg.msg_plainmsg("#3Error " + error);
          });
        }).catch((error) => {
          this.msg.msg_plainmsg("#2Error " + error);
        });
    }).catch((error) => {
      this.msg.msg_plainmsg("#1Error " + error);
    });
  }

  openlayerfoto(jenis,data){
    this.navCtrl.push(UploadPage, {
      data :data,
      jenis :jenis,
      Kode_Karyawan:this.id_karyawan,
      datawo:this.data_wo,
      id_dashboard_detail:this.id_dashboardetail
    });
  }
}
