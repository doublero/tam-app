import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DashboardwoPage } from './dashboardwo';

@NgModule({
  declarations: [
    DashboardwoPage,
  ],
  imports: [
    IonicPageModule.forChild(DashboardwoPage),
  ],
})
export class DashboardwoPageModule {}

