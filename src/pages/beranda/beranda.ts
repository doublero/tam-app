import { Component } from '@angular/core';
import { Platform, IonicPage, ToastController, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { MsgProvider } from '../../providers/msg/msg';
import { MenuController } from 'ionic-angular';
import { FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/debounceTime';

import { ServiceProvider } from '../../providers/service/service';
import { LocalstoragecontrollerProvider } from '../../providers/localstoragecontroller/localstoragecontroller';
import { FindarrProvider } from '../../providers/findarr/findarr';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { DashboardwoPage } from '../dashboardwo/dashboardwo';
import { CodePush, SyncStatus } from '@ionic-native/code-push';
import { DatabaseProvider } from '../../providers/database/database';
import { SQLiteObject } from '@ionic-native/sqlite';
import { ScanlibProvider } from '../../providers/scanlib/scanlib';
import { Network } from '@ionic-native/network';
@IonicPage()
@Component({
  selector: 'page-beranda',
  templateUrl: 'beranda.html'
})
export class BerandaPage {

  static get parameters() {
    return [
      [Platform],
      [ToastController],
      [Storage],
      [MsgProvider],
      [ServiceProvider],
      [MenuController],
      [LoadingController],
      [LocalstoragecontrollerProvider],
      [FindarrProvider],
      [BarcodeScanner],
      [NativeGeocoder],
      [Geolocation],
      [LocationAccuracy],
      [NavController],
      [NavParams],
      [CodePush],
      [DatabaseProvider],
      [ScanlibProvider],
      [Network]
    ]
  }
  searchControl: FormControl;
  public menus: any;
  public userdata: any;
  public apikeyaccess: any;
  public datateam: any;
  public searching: any = false;
  private loading: Loading;
  public isoffline: boolean = false;
  state: any;
  colstate: any;
  nik: any;
  nama: any;
  dataSet: any;
  dataretrieve: any;
  searchTerm: string = '';
  count_not_yet: number; count_on_progress: number; count_closed: number;
  datalocal: any = [];
  datawo = {};
  lat: any;
  lng: any;
  lokasi: any;
  constructor(
    private platform,
    private toast,
    private storage,
    private msg,
    private service,
    private menuCtrl,
    private loadingCtrl,
    private localctrl,
    private findarr,
    private scanner,
    private nativeGeocoder,
    private geo,
    private locationAccuracy,
    public navCtrl,
    public navParams,
    private codePush,
    private db,
    public scans,
    private network
  ) {
    this.platform.ready().then(() => {
      this.codePush.sync({}, (progress) => []).subscribe((status) => {
        if (status == SyncStatus.UPDATE_INSTALLED)
          // alert("Aplikasi berhasil diupdate")
          this.msg.msg_plainmsg("Aplikasi Berhasil Update Otomatis, Harap Tutup Apkilasi Lalu Buka Kembali");
        if (status == SyncStatus.ERROR)
        this.msg.msg_plainmsg("Aplikasi Gagal Update Otomatis, Harap Tutup Apkilasi Lalu Buka Kembali");
      });
    });

    this.searchControl = new FormControl();
    this.storage.get('module_endpoint').then((val) => {
      this.menus = val;
    });
    this.storage.get('userdatateknisi').then((val) => {
      this.userdata = val;
      this.nik = this.userdata[0]["nik"];
      this.nama = this.userdata[0]["Nama_Karyawan"];
    });
    this.storage.get('apikeyaccess').then((val) => {
      this.apikeyaccess = val;
    });
    this.storage.get('datateam').then((val) => {
      this.datateam = val;
    });
  }
  changestate() {
    if (this.isoffline == false) {
      this.toast.create({ message: "Pulling live data", duration: 3000, position: 'top' }).present();
      this.state = 'Online';
      this.colstate = 'secondary';
      this.loaddatasearch();
    } else {
      this.toast.create({ message: "Pulling local data", duration: 3000, position: 'top' }).present();

      this.state = 'Offline';
      this.colstate = 'danger';
      this.loadDatalocal();
    }
  }
  doRefresh(event) {
    this.dataSet = null;
    if (this.isoffline == false) {
      this.loaddatasearch();
    } else {
      this.loadDatalocal();
    }
    setTimeout(() => {
      event.complete();
    }, 1000);
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true, 'myMenu');
  }
  getonlyteam(data) {
    if (data != null) {
      let maksloop = data.length;
      var team = [];
      for (var i = 0; i < maksloop; i++) {
        team.push(data[i]["id_team"]);
      }
      return team;
    }
  }
  deletelocal() {
    this.db.getOrGenerateDB().then((db: SQLiteObject) => {
      db.executeSql('DELETE FROM work_order', []).then(datatable => {
        this.msg.msg_plainmsg("DATA DIHAPUS");
      });
    }).catch(e => this.msg.msg_plainmsg(JSON.stringify(e)));

  }
  loadDatalocal() {

    this.db.getOrGenerateDB().then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM work_order', []).then(datatable => {
        this.dataSet = [];
        if (datatable.rows.length > 0) {
          for (var i = 0; i < datatable.rows.length; i++) {
            this.dataSet.push({
              no_wo: datatable.rows.item(i).no_wo,
              id_wo: datatable.rows.item(i).id_wo,
              id_jenis_kunjungan: datatable.rows.item(i).id_jenis_kunjungan,
              keterangan: datatable.rows.item(i).keterangan,
              datecreated: datatable.rows.item(i).datecreated,
              STATUS: datatable.rows.item(i).STATUS,
              sign_path: datatable.rows.item(i).sign_path,
              id_rs: datatable.rows.item(i).id_rs,
              approved: datatable.rows.item(i).approved,
              approved_by: datatable.rows.item(i).approved_by,
              kode_karyawan: datatable.rows.item(i).kode_karyawan,
              Nama_customer: datatable.rows.item(i).Nama_customer,
              kode_customer: datatable.rows.item(i).kode_customer,
              deskripsi: null
            });
          }
        } else {
          this.msg.msg_plainmsg("Data kosong");
        }
      }).catch(e => this.msg.msg_plainmsg(JSON.stringify(e)));
    });
  }
  loaddatasearch() {
    if (this.isoffline == false) {
      this.state = 'Online';
      this.colstate = 'secondary';
      this.storage.get('dynamicurl_utama').then((val) => {
        if (val != null) {
          var nextsegment = null;
          if (this.findarr.searchmenu("CountWO", this.menus) != null) {
            nextsegment = this.findarr.searchmenu("CountWO", this.menus);
            var dataarr = this.getonlyteam(this.datateam);
            var datapostcount = {
              "apikey": this.apikeyaccess,
              "id_team": dataarr
            }
            this.createLoader('Loading ...');
            this.loading.present().then(() => {
              this.dissmissloading();
              this.service.postData(datapostcount, val, nextsegment["endpoint"]).then((result) => {
                if (result["message"] == true) {
                  if (result["data"] == "") {
                    this.count_not_yet = 0;
                    this.count_on_progress = 0;
                    this.count_closed = 0;
                  } else {
                    this.count_not_yet = result["data"][0].notyet;
                    this.count_on_progress = result["data"][0].onprocces;
                    this.count_closed = result["data"][0].done;
                  }
                  nextsegment = this.findarr.searchmenu("GetAll", this.menus);
                  this.service.postData(datapostcount, val, nextsegment["endpoint"]).then((result) => {
                    if (result["message"] == true) {
                      this.dataretrieve = result['data'];
                      this.searching = false;
                      this.dataSet = this.filterItems(this.searchTerm);
                      this.searching = false;
                      if (this.dataSet == null || this.dataSet == "") {
                        this.msg.msg_plainmsg('data not available');
                      } else {
                        this.dataSet.forEach(dataobj => {
                          var no_wo = dataobj.no_wo;
                          var id_tiket = dataobj.id_tiket;
                          var id_jenis_kunjungan = dataobj.id_jenis_kunjungan;
                          var keterangan = dataobj.keterangan;
                          var datecreated = dataobj.datecreated;
                          var STATUS = dataobj.STATUS;
                          var sign_path = dataobj.sign_path;
                          var id_rs = dataobj.id_rs;
                          var kode_customer = dataobj.kode_customer;
                          var Nama_customer = dataobj.Nama_customer;
                          var approved = dataobj.approved;
                          var approved_by = dataobj.approved_by;
                          var kode_karyawan = dataobj.kode_karyawan;

                          this.db.getOrGenerateDB().then((db: SQLiteObject) => {
                            db.executeSql('SELECT * FROM work_order WHERE no_wo = ?', [no_wo]).then(datatable => {
                              this.datalocal = [];
                              if (datatable.rows.length == 0) {
                                db.executeSql(
                                  "INSERT INTO work_order (no_wo,id_tiket,id_jenis_kunjungan,keterangan,datecreated,STATUS,sign_path,id_rs,kode_customer,Nama_customer,approved,approved_by,kode_karyawan)" +
                                  " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)",
                                  [
                                    no_wo,
                                    id_tiket,
                                    id_jenis_kunjungan,
                                    keterangan,
                                    datecreated,
                                    STATUS,
                                    sign_path,
                                    id_rs,
                                    kode_customer,
                                    Nama_customer,
                                    approved,
                                    approved_by,
                                    kode_karyawan]
                                ).then(() => {
                                  console.log('berhasil menyimpan data');
                                }).catch(e => {
                                  this.msg.msg_plainmsg(JSON.stringify(e));
                                });
                              } else {
                                db.executeSql("UPDATE work_order SET " +
                                  "no_wo = ?,id_tiket=?,id_jenis_kunjungan=?,keterangan=?,datecreated=?,STATUS=?," +
                                  "sign_path=?,id_rs=?,kode_customer=?,Nama_customer=?,approved=?,approved_by=?," +
                                  "kode_karyawan=? WHERE no_wo = ?",
                                  [no_wo, id_tiket, id_jenis_kunjungan, keterangan, datecreated, STATUS, sign_path,
                                    id_rs, kode_customer, Nama_customer, approved, approved_by, kode_karyawan, no_wo]
                                ).then(() => {
                                  console.log("Berhasil mengupdate data!");
                                }).catch(e => {
                                  this.msg.msg_plainmsg(JSON.stringify(e));
                                });
                              }
                            });
                          });
                        });
                      }

                    } else {
                      this.localctrl.restart();
                    }
                    this.dissmissloading();
                  });
                } else {
                  if (result["data"] == "Team Anda belum memiliki RS") {
                    this.msg.msg_plainmsg(result["data"])
                  } else {
                    this.localctrl.restart();
                  }
                }
              });
              setTimeout(() => {
                this.dissmissloading();
              }, 5000);
            }, (err) => {
              this.dissmissloading();
              this.msg.msg_plainmsg(err);
            });
          }
        } else {
          this.localctrl.restart();
        }
      });

    } else {
      this.state = 'Offline';
      this.colstate = 'danger';
      this.loadDatalocal();
    }

  }

  ionViewDidLoad() {
    if (this.isoffline == true) {
      this.loadDatalocal();
      this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
        this.searching = false;
        this.loadDatalocal();
      });
    } else {
      this.loaddatasearch();
      this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
        this.searching = false;
        this.loaddatasearch();
      });
    }
  }
  onSearchInput() {
    this.searching = true;
  }
  filterItems(searchTerm) {
    if (searchTerm != null && searchTerm != "") {
      return this.dataretrieve.filter((item) => {
        return item.Nama_customer.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
      })
    } else {
      return this.dataretrieve;
    }
  }
  createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  scanbarcode(data) {
    if (this.isoffline == true) {
      this.scanner.scan().then(barcodeData => {
        if (barcodeData.text != "") {
          this.prosescheckofffline(barcodeData.text, data);
        }
      }).catch(err => {
        this.msg.msg_plainmsg(JSON.stringify(err));
      });
    } else {
      this.locationAccuracy.canRequest().then((canRequest: boolean) => {
        if (canRequest) {
          this.geo.getCurrentPosition().then((pos) => {
            this.lat = pos.coords.latitude;
            this.lng = pos.coords.longitude;
            this.nativeGeocoder.reverseGeocode(this.lat, this.lng)
              .then((result: NativeGeocoderReverseResult[]) => {
                this.lokasi = JSON.stringify(result);
                this.scanner.scan().then(barcodeData => {
                  if (barcodeData.text != "") {
                    this.prosescheck(barcodeData.text, this.lng, this.lat, this.lokasi, data);
                  }
                }).catch(err => {
                  this.msg.msg_plainmsg(JSON.stringify(err));
                });
              }).catch((error) => {
                this.msg.msg_plainmsg(JSON.stringify(error));
              });
          }).catch((error) => {
            this.msg.msg_plainmsg(JSON.stringify(error));
          });
        } else {
          this.msg.msg_plainmsg("Oppss.. Anda belum mengizinkan perangkat mengkses Lokasi, Caranya ke Pengaturan => Aplikasi => TAM App => Perizinan aplikasi");
        }
      });
    }

  }
  prosescheckofffline(barcode, data) {
    this.msg.msg_plainmsg("OFFLINE muncul");
    if (barcode != "") {
      let today: any = new Date();
      let dates: any = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
      let times: any = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
      let datetimes = dates + ' ' + times;
      if (barcode != data["kode_customer"]) {
        this.msg.msg_plainmsg("Barcode tidak sesuai dengan WorkOrder");
      } else {

        this.db.getOrGenerateDB().then((db: SQLiteObject) => {
          db.executeSql('INSERT INTO dashboard_wo_header (id_wo,open_time,closed_time,open_by,closed_by) ' +
            'VALUES(?,?,?,?,?)', [data["id_wo"], datetimes, null, data["kode_karyawan"], null]).then(() => {
              db.executeSql("SELECT * FROM dashboard_wo_header WHERE id_wo = ?",[data["id_wo"]]).then((results) => {
                if (results.rows.length > 0) {
                  db.executeSql("UPDATE work_order SET STATUS='1' WHERE id_wo=?",
                  [data["id_wo"]]).then(()=>{
                    db.executeSql("SELECT * FROM work_order ",[]).then((results) => {
                      if (results.rows.length > 0) {
                        this.msg.msg_plainmsg(data["id_wo"]);
                        this.msg.msg_plainmsg(JSON.stringify(results.rows.item(0)));
                      }else{
                        this.msg.msg_plainmsg("data kosong");
                      }
                    });
                  });
                }else{
                  this.msg.msg_plainmsg("data kosong");
                }
                }).catch(e => {
                  this.msg.msg_plainmsg(JSON.stringify(e));
                });
            }).catch(e => {
              this.msg.msg_plainmsg(JSON.stringify(e));
            });
          /*db.executeSql('INSERT INTO dashboard_wo_header (id_wo,open_time,closed_time,open_by,closed_by) '+
          'VALUES(?,?,?,?,?)', [data["id_wo"],datetimes,null,data["kode_karyawan"],null]).then(() => {
            db.executeSql("UPDATE work_order SET STATUS='1' WHERE id_wo=?",
            [data["id_wo"]]).then(()=>{
              db.executeSql("SELECT id_dashboard_wo_header FROM dashboard_wo_header WHERE id_wo = ? AND open_by=?",
              [data["id_wo"],data["kode_karyawan"]]).then((results)=>{
                this.msg.msg_plainmsg(JSON.stringify(results));
            })
          }).catch(e => {
            this.msg.msg_plainmsg(JSON.stringify(e));
          });
          }).catch(e => {
            this.msg.msg_plainmsg(JSON.stringify(e));
          });*/
        });
      }

    }
  }
  prosescheck(barcode, lng, lat, loc, data) {
    if (barcode != "" && lat != "" && lng != "") {
      let today: any = new Date();
      let dates: any = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
      let times: any = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
      let datetimes = dates + ' ' + times;
      this.storage.get('dynamicurl_utama').then((val) => {
        if (val != null) {
          var nextsegment = null;
          if (this.findarr.searchmenu("Scanheader", this.menus) != null) {
            nextsegment = this.findarr.searchmenu("Scanheader", this.menus);
            var postparam = {
              barcode: barcode,
              lng: lng,
              lat: lat,
              loc: loc,
              datetime: datetimes,
              "userdata": this.userdata,
              "apikey": this.apikeyaccess,
              "datatiket": data
            }
            this.createLoader('Posting Data ...');
            // this.msg.msg_plainmsg(JSON.stringify(postparam));
            this.loading.present().then(() => {

              this.service.postData(postparam, val, nextsegment["endpoint"]).then((result) => {
                if (result["message"] == true) {
                  this.toast.create({ message: result["data"], duration: 3000, position: 'top' }).present();
                  this.loaddatasearch();
                  this.dissmissloading();
                } else {
                  this.msg.msg_plainmsg(result["data"]);
                  this.dissmissloading();
                }
                this.dissmissloading();
              });

              setTimeout(() => {
                this.dissmissloading();
              }, 3000);
            }, (err) => {
              this.dissmissloading();
              this.msg.msg_plainmsg(err);
            });
          }
        } else {
          this.localctrl.restart();
        }
      });
    } else {
      this.msg.msg_plainmsg('Error while scanning barcode');
    }

  }
  detailwo(param) {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("PullingDashboard", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("PullingDashboard", this.menus);
          let datapostcount = {
            "apikey": this.apikeyaccess,
            "datawo": param
          };
          this.service.postData(datapostcount, val, nextsegment["endpoint"]).then((result) => {
            if (result["message"] == true) {
              this.navCtrl.push(DashboardwoPage, { "dashboardwoheader": result["data"], "datawo": param });
            } else {
              this.msg.msg_plainmsg(JSON.stringify(result["data"]));
              this.dissmissloading();
            }
          });
        }
      } else {
        this.localctrl.restart();
      }
    });

  }

  changeimg(param) {
    let imgsrc = new String;
    if (param == null || param == "") {
      imgsrc = "assets/imgs/iconcust.png";
    } else {
      imgsrc = param;
    }
    return imgsrc;
  }
  checkstatus(param) {
    let color = new String;
    if (param == '0') {//0 = UNREAD    ABU-ABU
      color = "onnotyet";
    } else if (param == '1') {//2 = PROGRESS	BIRU
      color = "onproccess";
    } else if (param == '2') {//   3 = CLOSE		  MERAH
      color = "onclose";
    }
    return color;
  }
  checkurgensiwrn(param) {
    let color = new String;
    if (param == '1') {//0 = UNREAD    ABU-ABU
      color = "energy";
    } else if (param == '2') {//2 = PROGRESS	BIRU
      color = "danger";
    }

    return color;
  }
  checkurgensi(param) {
    let txt = new String;
    if (param == '1') {//0 = blm proses    ABU-ABU
      txt = "IMPORTANT";
    } else if (param == '2') {
      txt = "URGENT";
    } else {
      txt = "";
    }
    return txt;
  }
  checktext(param) {
    let txt = new String;
    if (param == '0') {//0 = blm proses    ABU-ABU
      txt = "NOT YET";
    } else if (param == '1') {//2 = PROGRESS	BIRU
      txt = "ON PROCESS";
    } else if (param == '2') {//   3 = CLOSE		MERAH
      txt = "CLOSED";
    }
    return txt;
  }
  aktiftombol(param) {
    let statusz = new Boolean;
    if (param != '0') {//0 = UNREAD    ABU-ABU
      statusz = true;
    } else {
      statusz = false;
    }
    return statusz;
  }
  aktiftombolresume(param) {
    let statusz = new Boolean;
    if (param == '1') {//0 = UNREAD    ABU-ABU
      statusz = false;
    } else {
      statusz = true;
    }
    return statusz;
  }



}
