import { Component } from '@angular/core';
import { App, IonicPage, NavController, Platform, ActionSheetController, NavParams, LoadingController, Loading, Content, normalizeURL } from 'ionic-angular';
import { MsgProvider } from '../../providers/msg/msg';
import { Storage } from '@ionic/storage';
import { LoginsPage } from '../../pages/logins/logins';
import { MenuController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';

import { ApiProvider } from '../../providers/api/api';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import {BerandaPage} from '../beranda/beranda';
import {UploadPage} from '../upload/upload';
import { TtdwoPage } from '../../pages/ttdwo/ttdwo';
import { LocalstoragecontrollerProvider } from '../../providers/localstoragecontroller/localstoragecontroller';
import { FindarrProvider } from '../../providers/findarr/findarr';

@IonicPage()
@Component({
  selector: 'page-finaltask',
  templateUrl: 'finaltask.html',
})
export class FinaltaskPage {
  lat:any;
  lng:any;
  lokasi:any;
  dataSet: any;
  public datateknisi:any;
  loading: Loading;apikeyaccess:any;
  private datawo:any;
  private datadashboardheader:any;
  private iddashboarddetail:any;menus:any;
  constructor(
    private scanner: BarcodeScanner,
    private nativeGeocoder: NativeGeocoder,
    private geo: Geolocation,
    private transfer: FileTransfer,
    private locationAccuracy: LocationAccuracy,
    private camera: Camera,
    private file: File,
    private toast:ToastController,
    private api:ApiProvider,
    public platform: Platform,
    private storage: Storage,
    public menuCtrl: MenuController,
    public actionSheetCtrl: ActionSheetController,
    private app:App,
    private localctrl: LocalstoragecontrollerProvider,
    private findarr: FindarrProvider,
    private service:ServiceProvider,
    private msg: MsgProvider,
    private loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
   
    public navCtrl: NavController, public navParams: NavParams) {
      this.datawo = this.navParams.get('datawo');
      this.datadashboardheader= this.navParams.get('dashboardheader');
      this.iddashboarddetail= this.navParams.get('iddashboarddetail');
      this.datateknisi = this.navParams.get('datateknisi'); 
      this.storage.get('module_endpoint').then((val) => {
        this.menus = val;
      });
     
      this.storage.get('apikeyaccess').then((val) => {
        this.apikeyaccess = val;
      });
    }

  public createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  public dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FinaltaskPage');
  }
  ionViewWillEnter() {
    this.loaddata();
    this.menuCtrl.enable(false, 'myMenu');
  }
  loaddata() {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("getphotoscan", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("getphotoscan", this.menus);
          this.createLoader('Pulling Data ...');
          this.loading.present().then(() => {
            let datapost = {
              "data_wo": this.datawo,
              "apikey": this.apikeyaccess
            }
            this.service.postData(datapost, val, nextsegment["endpoint"]).then((result) => {
              if(result["message"]==true){
                this.dataSet = result["data"];
               }else{
                 this.msg.msg_plainmsg(JSON.stringify(result["data"]));
               }
               this.dissmissloading();
            });
          setTimeout(() => {
              this.dissmissloading();
            }, 5000);
          }, (err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          });
        }else{
          this.localctrl.restart();
        }
      }else{
        this.localctrl.restart();
      }
    });
  }
  
  checkstatus(param) {
    let status = new String;
    if (param == null || param == "") {
      status = "Belum Upload";
    } else {
      status = "Sudah Upload";
    }
    return status;
  }



  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  scanbarcode(){
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
         this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
           () => {
             this.geo.getCurrentPosition().then((pos) => {
               this.lat = pos.coords.latitude;
               this.lng = pos.coords.longitude;
               this.nativeGeocoder.reverseGeocode(this.lat, this.lng)
                 .then((result: NativeGeocoderReverseResult[]) => {
                  this.lokasi = JSON.stringify(result);
                   this.scanner.scan().then(barcodeData => {
                     if (barcodeData.text != "") {
                       this.prosescheck(barcodeData.text,this.lng,this.lat,this.lokasi,this.datawo);
                       this.dissmissloading();
                     }
                   }).catch(err => {
                     this.msg.msg_plainmsg(err);
                   });
                 })
                 .catch((error: any) => {
                   this.dissmissloading();
                   this.msg.msg_plainmsg("Error Native Geocoder " + error);
                 });
             }).catch((error) => {
               this.dissmissloading();
               this.msg.msg_plainmsg("GAGAL geolocation " + error);
             });
           },
           error => {
             this.dissmissloading();
             this.msg.msg_plainmsg("GAGAL locationaccuracy" + error);
           }
         );
     });
  }
  prosescheck(barcode,lng,lat,loc,datascan) {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("ClosedWo", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("ClosedWo", this.menus);
          let postparam = {
            barcode:barcode,
            lng:lng,
            lat:lat,
            loc:loc,
            datascan:datascan,
            "iddashboarddetail":this.iddashboarddetail,
            "userdata":this.datateknisi,
            "apikey": this.apikeyaccess
          }
          this.createLoader('Posting Data ...');
          this.loading.present().then(() => {
            this.service.postData(postparam, val, nextsegment["endpoint"]).then((result) => {
              if(result["message"] == true){
                this.toast.create({ message: result["data"], duration: 3000, position: 'top' }).present();
                this.dissmissloading();
                this.app.getRootNav().setRoot(BerandaPage);  
              }else{
                this.msg.msg_plainmsg(JSON.stringify(result["data"]));
                this.dissmissloading();
              }
            });
          setTimeout(() => {
                this.dissmissloading();
              }, 3000);
            }, (err) => {
              this.dissmissloading();
              this.msg.msg_plainmsg(err);
            });
        }else{
          this.localctrl.restart();
        }
      }else{
this.localctrl.restart();
      }
    });
  }
 
  signpageopen() {
    this.navCtrl.push(TtdwoPage, { datawo:this.datawo,datateknisi:this.datateknisi });
  }

  openlayerfoto(jenis,data){
    this.navCtrl.push(UploadPage, {
      data :data,
      jenis :jenis,
      datateknisiz:this.datateknisi,
      datawo:this.datawo,
      id_dashboard_detail:this.iddashboarddetail
    });
  }
}
