import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HeaderscanPage } from './headerscan';

@NgModule({
  declarations: [
    HeaderscanPage,
  ],
  imports: [
    IonicPageModule.forChild(HeaderscanPage),
  ],
})
export class HeaderscanPageModule {}
