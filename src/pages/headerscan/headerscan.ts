import { Component } from '@angular/core';
import { App, IonicPage, NavController, LoadingController, ActionSheetController, Loading, NavParams } from 'ionic-angular';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import { MsgProvider } from '../../providers/msg/msg';
import { Storage } from '@ionic/storage';
import { LoginsPage } from '../../pages/logins/logins';
import { ServiceProvider } from '../../providers/service/service';
import { DetailscanPage } from '../../pages/detailscan/detailscan';
import { ReqsparepartPage } from '../../pages/reqsparepart/reqsparepart';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { Geolocation } from '@ionic-native/geolocation';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ToastController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { FinaltaskPage } from '../../pages/finaltask/finaltask';
import { FindarrProvider } from '../../providers/findarr/findarr';
import { LocalstoragecontrollerProvider } from '../../providers/localstoragecontroller/localstoragecontroller';
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-headerscan',
  templateUrl: 'headerscan.html',
})
export class HeaderscanPage {
  menus: any; datateam: any; accesstxt: any; nama: any; apikeyaccess: any;
  nik: any; userdata: any;

  //---------------/
  public activebutton: any = false;
  datawo: any;
  dataSet: any;
  searchControl: FormControl;
  loading: Loading;
  searchTerm: string = '';
  dataretrieve: any;
  lat: any; lng: any; lokasi: any;
  datadashboardheader: any;
  iddashboarddetail: any;
  private datateknisi: any;
  public getdatateknisi(): any {
    return this.datateknisi;
  }
  public setdatateknisi(value: any) {
    this.datateknisi = value;
  }


  public searching: any = false;
  constructor(
    private alerts: AlertController,
    private findarr: FindarrProvider,
    private service: ServiceProvider,
    private app: App,
    public actionSheetCtrl: ActionSheetController,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    private msg: MsgProvider,
    private loadingCtrl: LoadingController,
    private nativeGeocoder: NativeGeocoder,
    private geo: Geolocation,
    private locationAccuracy: LocationAccuracy,
    private toast: ToastController,
    private scanner: BarcodeScanner,
    private api: ApiProvider,
    private localctrl: LocalstoragecontrollerProvider,

  ) {
    this.datawo = this.navParams.get('datawo');
    this.datadashboardheader = this.navParams.get('dashboardheader');
    this.iddashboarddetail = this.navParams.get('iddashboarddetail');
    this.searchControl = new FormControl();
    this.storage.get('module_endpoint').then((val) => {
      this.menus = val;
    });
    this.storage.get('userdatateknisi').then((val) => {
      this.userdata = val;
      this.nik = this.userdata[0]["nik"];
      this.nama = this.userdata[0]["Nama_Karyawan"];
    });
    this.storage.get('apikeyaccess').then((val) => {
      this.apikeyaccess = val;
    });
    this.storage.get('access').then((val) => {
      this.accesstxt = val;
    });
    this.storage.get('datateam').then((val) => {
      this.datateam = val;
    });
  }
  public createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }

  dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  onSearchInput() {
    this.searching = true;
  }
  filterItems(searchTerm) {
    if (searchTerm != null && searchTerm != "") {
      return this.dataretrieve.filter((item) => {
        return item.no_seri.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
      })
    } else {
      return this.dataretrieve;
    }
  }
  checkstatus(param) {
    let color = new String;
    if (param == '0') {//0 = UNREAD    ABU-ABU
      color = "onnotyet";
    } else if (param == '1') {//2 = PROGRESS	BIRU
      color = "onproccess";
    } else if (param == '2') {//   3 = CLOSE		  MERAH
      color = "onclose";
    }
    return color;
  }
  checkurgensiwrn(param) {
    let color = new String;
    if (param == '1') {//0 = UNREAD    ABU-ABU
      color = "energy";
    } else if (param == '2') {//2 = PROGRESS	BIRU
      color = "danger";
    }
    return color;
  }
  checktext(param) {
    let txt = new String;
    if (param == '0') {//0 = blm proses    ABU-ABU
      txt = "NOT YET";
    } else if (param == '1') {//2 = PROGRESS	BIRU
      txt = "ON PROCESS";
    } else if (param == '2') {//   3 = CLOSE		MERAH
      txt = "CLOSED";
    }
    return txt;
  }
  checkurgensi(param) {
    let txt = new String;
    if (param == '1') {//0 = blm proses    ABU-ABU
      txt = "IMPORTANT";
    } else if (param == '2') {
      txt = "URGENT";
    } else {
      txt = "";
    }
    return txt;
  }
  changeimg(param) {
    let imgsrc = new String;
    if (param == null || param == "") {
      imgsrc = "assets/imgs/iconcust.png";
    } else {
      imgsrc = param;
    }
    return imgsrc;
  }
  ionViewDidLoad() {
    this.loaddatasearch();
    this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
      this.searching = false;
      this.loaddatasearch();
    });
  }
  doRefresh(event) {
    this.dataSet = null;
    this.loaddatasearch();
    setTimeout(() => {
      event.complete();
    }, 1000);
  }
  loaddatasearch() {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("Pullscanmesin", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("Pullscanmesin", this.menus);
          var datapostcount = {
            "apikey": this.apikeyaccess,
            "dataheaderwo": this.datawo
          }
          this.createLoader('Pulling Data ...');
          this.loading.present().then(() => {
            this.service.postData(datapostcount, val, nextsegment["endpoint"]).then((result) => {
              if (result['data'] != "") {
                this.activebutton = false;
              } else {
                this.activebutton = true;
              }
              let datauser = result['data'];
              this.dataretrieve = datauser;
              this.searching = false;
              this.dataSet = this.filterItems(this.searchTerm);
              this.dissmissloading();
            });
            setTimeout(() => {
              this.dissmissloading();
            }, 3000);
          }, (err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          });
        } else {
          this.localctrl.restart();
        }
      } else {
        this.localctrl.restart();
      }
    });
  }


  scanbarcode() { //CORDOVA

    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        () => {
          this.geo.getCurrentPosition().then((pos) => {
            this.lat = pos.coords.latitude;
            this.lng = pos.coords.longitude;
            this.nativeGeocoder.reverseGeocode(this.lat, this.lng)
              .then((result: NativeGeocoderReverseResult[]) => {
                this.lokasi = JSON.stringify(result);
                this.scanner.scan().then(barcodeData => {
                  if (barcodeData.text != "") {
                    let datascan = {
                      'id_dashboard_detail': this.iddashboarddetail,
                      'kode_customer': this.datawo['kode_customer']
                    };
                    this.prosescheck(barcodeData.text, this.lng, this.lat, this.lokasi, datascan);

                  }
                }).catch(err => {
                  this.msg.msg_plainmsg(err);
                });
              })
              .catch((error: any) => {
                this.msg.msg_plainmsg("Error Native Geocoder " + error);
              });
          }).catch((error) => {
            this.msg.msg_plainmsg("GAGAL geolocation " + error);
          });
        },
        error => {
          this.msg.msg_plainmsg("GAGAL locationaccuracy" + error);
        }
      );
    });

  }

  prosescheck(barcode, lng, lat, lokasi, datascan) {
    if (barcode != "" && lat != "" && lng != "") {
      this.storage.get('dynamicurl_utama').then((val) => {
        if (val != null) {
          var nextsegment = null;
          if (this.findarr.searchmenu("Scanmesin", this.menus) != null) {
            nextsegment = this.findarr.searchmenu("Scanmesin", this.menus);
            let postparam = {
              barcode: barcode,
              lng: lng,
              lat: lat,
              loc: lokasi,
              no_ref: this.datawo["no_wo"],
              datascan: datascan,//nowo
              "apikey": this.apikeyaccess
            }
            this.createLoader('Posting Data ...');
            this.loading.present().then(() => {
              this.service.postData(postparam, val, nextsegment["endpoint"]).then((result) => {
                if (result["message"] == true) {
                  this.toast.create({ message: result["data"], duration: 3000, position: 'top' }).present();
                  this.loaddatasearch();
                  this.dissmissloading();
                } else {
                  this.msg.msg_plainmsg(JSON.stringify(result["data"]));
                  this.dissmissloading();
                }
              });
              setTimeout(() => {
                this.dissmissloading();
              }, 3000);
            }, (err) => {
              this.dissmissloading();
              this.msg.msg_plainmsg(err);
            });
          }
        } else {
          this.localctrl.restart();
        }
      });
    }

  }
  showprompt(dataparam) {
    const confirm = this.alerts.create({
      title: 'Logout Aplikasi',
      message: 'Apakah Anda yakin ingin menghapus data scan?',
      buttons: [
        {
          text: 'Ya',
          handler: () => {
            this.hapus(dataparam);
          }
        },
        {
          text: 'Tidak',
          handler: () => {
            //ga ngapa2in
          }
        }
      ]
    });
    confirm.present();
  }
  hapus(data) {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("DeleteScanMesin", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("DeleteScanMesin", this.menus);
          var datapostcount = {
            "apikey": this.apikeyaccess,
            "data": data,
            "no_ref": this.datawo["no_wo"],
            "iddashboarddetail": this.iddashboarddetail,
          }
          this.createLoader('Deleting data ...');
          this.loading.present().then(() => {
            this.service.postData(datapostcount, val, nextsegment["endpoint"]).then((result) => {
              this.dissmissloading();
              this.loaddatasearch();
            });
            setTimeout(() => {
              this.dissmissloading();
            }, 3000);
          }, (err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          });
        } else {
          this.localctrl.restart();
        }
      } else {
        this.localctrl.restart();
      }
    });
  
  }

  resume(data) {
    this.navCtrl.push(DetailscanPage, { datascan: data });
  }
  requestsp() {
    this.navCtrl.push(ReqsparepartPage, {
      'data_wo': this.datawo,
      'iddashboarddetail': this.iddashboarddetail
    });
  }
  nextinput() {
    this.navCtrl.push(FinaltaskPage, {
      "datateknisi": this.userdata,
      "datawo": this.datawo,
      "datadashboardheader": this.datadashboardheader,
      "iddashboarddetail": this.iddashboarddetail
    });
  }
}
