import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProginstalasiPage } from './proginstalasi';

@NgModule({
  declarations: [
    ProginstalasiPage,
  ],
  imports: [
    IonicPageModule.forChild(ProginstalasiPage),
  ],
})
export class ProginstalasiPageModule {}
