import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import $ from 'jquery';
import { KomisioningroPage } from '../komisioningro/komisioningro';
import { KomisioninghdPage } from '../komisioninghd/komisioninghd';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { FinalinstalasiPage } from '../finalinstalasi/finalinstalasi';
import { MsgProvider } from '../../providers/msg/msg';

/**
 * Generated class for the ProginstalasiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-proginstalasi',
  templateUrl: 'proginstalasi.html',
})
export class ProginstalasiPage {

  nowo: any;
  datamesin: any;
  jmlmesin: any;
  sudahupload: any;
  datamesinupload: any;
  lat: any;
  lang: any;
  lokasi: any;
  disDone: any;
  public loading: Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private locationAccuracy: LocationAccuracy,
    public geo: Geolocation,
    private http: Http,
    public nativeGeocoder: NativeGeocoder,
    private scanner: BarcodeScanner,
    public loadingCtrl: LoadingController,
    public msg: MsgProvider
    ) {
      // this.nowo = this.navParams.get('nowo');
      // this.getData();
  }

  ionViewDidEnter() {
    this.nowo = this.navParams.get('nowo');
    this.getData();
    
    if (this.sudahupload > 0 && this.jmlmesin != 0) {
      this.disDone = true;
    }else if(this.sudahupload == 0 && this.jmlmesin == 0){
      this.disDone = true;
    }
  }

  getData(){
    this.createLoader('Memuat Data ...');
    this.loading.present().then(() => {
      this.dissmissloading();
      var nowo = this.navParams.get('nowo');
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.get('http://35.240.151.223:43209/newapimobile/get-data-instalasi-mesin/'+nowo).map(res => res.json()).subscribe(result => {
        var data = result['data'];
        this.datamesin = result['data'];
        this.jmlmesin = data.length;
      });
      
      this.http.get('http://35.240.151.223:43209/newapimobile/get-data-instalasi-mesin2/'+nowo).map(res => res.json()).subscribe(result => {
        var data = result['data'];
        this.datamesinupload = result['data'];
        this.sudahupload = data.length;
      });
    });
  }

  inputHD(param){
    let datapost = {
      "noref": this.nowo,
      "noseri": param.nomesin,
    }
    let headers = new Headers();
      headers.append('Content-Type', 'application/json');
    this.http.post('http://35.240.151.223:43209/newapimobile/get-scan-header', datapost, {headers: headers}).map(res => res.json()).subscribe(result => {
      var status = result['status'];
      var data = result['data'];
      if (status == true && data.length > 0) {
        let datapush = {
          "nomesin": param.nomesin,
          "nowo": this.nowo
        }
        this.navCtrl.push(KomisioningroPage, datapush);
      }else{
        this.locationAccuracy.canRequest().then((canRequest: boolean) => {
          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
            () => {
              this.geo.getCurrentPosition().then((pos) => {
                this.lat = pos.coords.latitude;
                this.lang = pos.coords.longitude;
                this.nativeGeocoder.reverseGeocode(this.lat, this.lang)
                  .then((result: NativeGeocoderReverseResult[]) => {
                    this.lokasi = JSON.stringify(result);
                    this.scanner.scan().then(barcodeData => {
                      if (barcodeData.text != "" && barcodeData.text == param.nomesin) {
                        let datapost = {
                          "nowo": this.nowo
                        }
                        this.http.post('http://35.240.151.223:43209/newapimobile/get-id-wd', datapost, {headers: headers}).map(res => res.json()).subscribe(hasil => {
                          var data = hasil['data'];
                          var idwd = data[0]['idwd'];
                          let datawd = {
                            "idwd": idwd,
                            "nowo": this.nowo,
                            "noseri": param.nomesin,
                            "jenismesin": 1,
                            "lat": JSON.parse(this.lokasi)[0].latitude,
                            "lang": JSON.parse(this.lokasi)[0].longitude,
                            "jalan": JSON.parse(this.lokasi)[0].thoroughfare,
                            "prov": JSON.parse(this.lokasi)[0].administrativeArea,                        "kodepos": JSON.parse(this.lokasi)[0].postalCode
                          }
                          this.navCtrl.push(KomisioninghdPage, {'dataparam': datawd});
                        });
                      }else{
                        // alert('Uppps... \n No. Seri mesin tidak sesuai !!');
                        this.msg.msg_plainmsg('No. Seri mesin tidak sesuai !!');
                      }
                    }).catch(err => {
                      alert(err);
                    });
                  }).catch((error) => {
                    alert("#4Error " + error);
                  });
              }).catch((error) => {
                alert("#3Error " + error);
              });
            }).catch((error) => {
              alert("#2Error " + error);
            });
        }).catch((error) => {
          alert("#1Error \n\n" + error);
        });
      }
    });
  }

  inputRO(param){
    let datapost = {
      "noref": this.nowo,
      "nomesin": param.nomesin,
    }
    let headers = new Headers();
      headers.append('Content-Type', 'application/json');
    this.http.post('http://35.240.151.223:43209/newapimobile/get-scan-header', datapost, {headers: headers}).map(res => res.json()).subscribe(result => {
      var status = result['status'];
      var data = result['data'];
      if (status == true && data.length > 0) {
        let datapush = {
          "nomesin": param.nomesin,
          "nowo": this.nowo
        }
        this.navCtrl.push(KomisioningroPage, datapush);
      }else{
        this.locationAccuracy.canRequest().then((canRequest: boolean) => {
          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
            () => {
              this.geo.getCurrentPosition().then((pos) => {
                this.lat = pos.coords.latitude;
                this.lang = pos.coords.longitude;
                this.nativeGeocoder.reverseGeocode(this.lat, this.lang)
                  .then((result: NativeGeocoderReverseResult[]) => {
                    this.lokasi = JSON.stringify(result);
                    this.scanner.scan().then(barcodeData => {
                      if (barcodeData.text != "" && barcodeData.text == param.nomesin) {
                        let datapost = {
                          "nowo": this.nowo
                        }
                        this.http.post('http://35.240.151.223:43209/newapimobile/get-id-wd', datapost, {headers: headers}).map(res => res.json()).subscribe(hasil => {
                          var data = hasil['data'];
                          var idwd = data[0]['idwd'];
                          let datawd = {
                            "idwd": idwd,
                            "nowo": this.nowo,
                            "noseri": param.nomesin,
                            "jenismesin": 2,
                            "lat": JSON.parse(this.lokasi)[0].latitude,
                            "lang": JSON.parse(this.lokasi)[0].longitude,
                            "jalan": JSON.parse(this.lokasi)[0].thoroughfare,
                            "prov": JSON.parse(this.lokasi)[0].administrativeArea,                        "kodepos": JSON.parse(this.lokasi)[0].postalCode
                          }
                          this.navCtrl.push(KomisioningroPage, {'dataparam': datawd});
                        });
                      }else{
                        this.msg.msg_plainmsg('No. Seri mesin tidak sesuai !!');
                      }
                    }).catch(err => {
                      alert(err);
                    });
                  }).catch((error) => {
                    alert("#4Error " + error);
                  });
              }).catch((error) => {
                alert("#3Error " + error);
              });
            }).catch((error) => {
              alert("#2Error " + error);
            });
        }).catch((error) => {
          alert("#1Error \n\n" + error);
        });
      }
    });
  }

  getStatusMesin(param){
    var nowo = nowo;
  }

  refresh(event){
    this.datamesin = null;
    this.jmlmesin = null;
    this.datamesinupload = null;
    this.sudahupload = null;
    this.getData();
    setTimeout(() => {
      console.log('berhasil merefresh data');
      event.complete();
    }, 1000);
  }

  createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  btnDone(){
    // alert(this.sudahupload+', '+this.jmlmesin);
    if (this.sudahupload == 0 && this.jmlmesin != 0 || this.sudahupload > 0 && this.jmlmesin != 0) {
      this.msg.msg_plainmsg('Belum instalasi semua mesin !!');
    }else{
      let postParam = {
        "nowo": this.nowo
      }
      this.navCtrl.push(FinalinstalasiPage, postParam);
    }
  }

}
