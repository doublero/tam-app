import { Component } from '@angular/core';
import { IonicPage,App, NavController, NavParams,Loading, LoadingController } from 'ionic-angular';
import { LoginsPage } from '../logins/logins';
import { MsgProvider } from '../../providers/msg/msg';
import { MenuController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-forgot',
  templateUrl: 'forgot.html',
})
export class ForgotPage {

  constructor(
    private menuCtrl:MenuController,
    private app:App,
    public navCtrl: NavController, 
    private msg:MsgProvider,
    public navParams: NavParams) {
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(false, 'myMenu');
  }
  forgot(){
    this.msg.msg_plainmsg("Aplikasi masih dalam tahap pengembangan");
  }
  back(){
    this.app.getRootNav().setRoot(LoginsPage);    
  }

}
