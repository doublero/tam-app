import { Component, ViewChild } from '@angular/core';
import { IonicPage, App, NavController, NavParams, ToastController, LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { IonicSelectableComponent } from 'ionic-selectable';
import { ServiceProvider } from '../../providers/service/service';
import { MsgProvider } from '../../providers/msg/msg';
import { ApiProvider } from '../../providers/api/api';
import { LoginsPage } from '../logins/logins';

import { FindarrProvider } from '../../providers/findarr/findarr';
import { LocalstoragecontrollerProvider } from '../../providers/localstoragecontroller/localstoragecontroller';
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-adderror',
  templateUrl: 'adderror.html',
})
export class AdderrorPage {
  menus: any; userdata: any; apikeyaccess: any;
  @ViewChild('myselect') selectComponent: IonicSelectableComponent;
  loading: Loading;
  ErrorList = [];
  ErrorId: any;
  no_seri: any;
  no_ref: any;
  id_dashboard_detail: any;
  id_scan_header: any;
  kode_karyawan: any;
  dataset: any;
  qtyjml: any;
  constructor(
    private alerts: AlertController,
    private findarr: FindarrProvider,
    private localctrl: LocalstoragecontrollerProvider,

    private app: App,
    private api: ApiProvider,
    private msg: MsgProvider,
    public navCtrl: NavController,
    private storage: Storage,
    private service: ServiceProvider,
    private toast: ToastController,
    private loadingCtrl: LoadingController,
    public navParams: NavParams) {
    this.no_seri = this.navParams.get('no_seri');
    this.no_ref = this.navParams.get('no_ref');
    this.id_dashboard_detail = this.navParams.get('id_dashboard_detail');
    this.id_scan_header = this.navParams.get('id_scan_header');
    this.storage.get('module_endpoint').then((val) => {
      this.menus = val;
    });
    this.storage.get('userdatateknisi').then((val) => {
      this.userdata = val;
    });
    this.storage.get('apikeyaccess').then((val) => {
      this.apikeyaccess = val;
    });
  }
  ErrorChanged(event: { component: IonicSelectableComponent, value: any }) {
    console.log('event :', event);
  }
  onClose(param) {
    let toatsz = this.toast.create({
      message: 'Thanks for your selection',
      duration: 2000
    });
    toatsz.present();
  }
  openFromCode() {
    this.selectComponent.open();
  }
  public createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  public dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  ionViewDidLoad() {
    this.getcomerror();
  }
  getcomerror() {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("LoadDataError", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("LoadDataError", this.menus);
          let datapost = {
            "keywords": "pulling_data_error",
            "apikey": this.apikeyaccess,
          }
          this.storage.get('data_error').then((resulterr) => {
            if (resulterr != null) {
              this.ErrorList = resulterr;
            } else {
              this.service.postData(datapost, val, nextsegment["endpoint"]).then((result) => {
                if (result["message"] == true) {
                  if (result["type"] == "data_error") {
                    let getdat = result['data'];
                    this.ErrorList = getdat;
                    this.storage.set('data_error', getdat);
                  }
                } else {
                  this.msg.msg_plainmsg(JSON.stringify(result['data']));
                }
              });
            }
          });
        } else {
          this.localctrl.restart();
        }
      } else {
        this.localctrl.restart();
      }
    });
  }
  ionViewWillEnter() {
    this.loadError();
  }
  loadError() {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("LoaddatatableError", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("LoaddatatableError", this.menus);
          let postparam = {
            id_scan_header: this.id_scan_header,
            "apikey": this.apikeyaccess
          }
          this.createLoader('Pulling Data ...');
          this.loading.present().then(() => {
            this.service.postData(postparam, val, nextsegment["endpoint"]).then((result) => {
         
              if (result["message"] == true) {
                this.dataset = result["data"];
                this.qtyjml = result["data"].length;
              } else {
                this.msg.msg_plainmsg(JSON.stringify(result["data"]));
              }
              this.dissmissloading();

            });
            setTimeout(() => {
              this.dissmissloading();
            }, 3000);
          }, (err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          });

        } else {
          this.localctrl.restart();
        }
      } else {
        this.localctrl.restart();
      }
    });

  }
  simpanerror() {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("SaveError", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("SaveError", this.menus);
          if (this.ErrorId != null) {
            let postparam = {
              id_scan_header: this.id_scan_header,
              kd_error: this.ErrorId,
              "apikey": this.apikeyaccess,
              "no_ref": this.no_ref,
              "id_dashboard_detail": this.id_dashboard_detail,
              "kode_karyawan": this.kode_karyawan
            }
            this.createLoader('Saving Data ...');
            this.loading.present().then(() => {
              this.service.postData(postparam, val, nextsegment["endpoint"]).then((result) => {
                if (result["message"] == true) {
                  this.toast.create({ message: result["data"], duration: 3000, position: 'top' }).present();
                  this.navCtrl.pop();
                } else {
                  this.msg.msg_plainmsg(JSON.stringify(result["data"]));
                }
              });
              setTimeout(() => {
                this.dissmissloading();
              }, 3000);
            }, (err) => {
              this.dissmissloading();
              this.msg.msg_plainmsg(err);
            });
          }
        } else {
          this.localctrl.restart();
        }
      } else {
        this.localctrl.restart();
      }
    });
  }
  deleteError(data) {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("DeleteError", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("DeleteError", this.menus);
          let postparam = {
            data: data,
            "apikey": this.apikeyaccess
          }
          this.service.postData(postparam, val, nextsegment["endpoint"]).then((result) => {
           if (result["message"] == true) {
              this.toast.create({ message: result["data"], duration: 3000, position: 'top' }).present();
              this.loadError();
            } else {
              this.msg.msg_plainmsg(JSON.stringify(result["data"]));
            }
          });
        } else {
          this.localctrl.restart();
        }
      } else {
        this.localctrl.restart();
      }
    });

  }

  showprompt(dataparam) {
    const confirm = this.alerts.create({
      title: 'Delete Data',
      message: 'Apakah Anda yakin ingin menghapus data ' + dataparam.errorlist + '?',
      buttons: [
        {
          text: 'Ya',
          handler: () => {
            this.deleteError(dataparam);
          }
        },
        {
          text: 'Tidak',
          handler: () => {
            //ga ngapa2in
          }
        }
      ]
    });
    confirm.present();
  }
}

