import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdderrorPage } from './adderror';

@NgModule({
  declarations: [
    AdderrorPage,
  ],
  imports: [
    IonicPageModule.forChild(AdderrorPage),
  ],
})
export class AdderrorPageModule {}
